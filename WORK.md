## Archivo para el registro de bugs y features necesarias para el proyecto.

### Bugs.

### Features.
- Realizar las siguientes correcciones al generador de modelos:
    [X] Eliminar espacios al final de las lineas cuando se genera.
    [X] Eliminar lineas innecesarias en los comentarios y mejorar la documentación.
    [X] Eliminar lineas innecesarias en los métodos generados para mantener la relación (Guia código limpio).
    [X] Añadir linea de separación entre los métodos de definición de relaciones.
    [X] Mejorar descripciones y formulario de generación.
- Realizar las siguientes correcciones al generador de CRUD´s:
    * Error en la ruta de los modelos que están en subdirectorios.
    * Error cuando las columnas de administración no existen.
    * Mejorar comentarios en modelos para la búsqueda.
- Realizar las siguientes correcciones al generador de Rest API:
    * Por defecto el identificador del módulo no debe ser ApiSoundMarket.
    * Mejorar comentarios y estructura de los controladores generados.
    * Mejorar comentarios y estructura de los modelos generados.
- Crear un generador de módulos.
* Añadir un nuevo generador de controladores.
* Crear un generador de CRUD que permita por medio de un archivo JSON pasar más configuraciones especificas y así hacerlo más personalizable.

## En proceso.
* Modificaciónes en vistas de generadores. (Empezando con el de modelo).