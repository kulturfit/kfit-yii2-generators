<?php

namespace kfit\generators\controllers;

use Yii;
/**
* Controlador por defecto.
*
* @package kfit
* @subpackage generators\controllers
* @category Controller
*
* @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
* @copyright Copyright (c) 2018 KulturFit S.A.S.
* @version 0.0.1
* @since 2.0.0
*/
class DefaultController extends \yii\gii\controllers\DefaultController
{
    public function actionView($id)
    {
        $generator = $this->loadGenerator($id);
        $params = ['generator' => $generator, 'id' => $id];
        $preview = Yii::$app->request->post('preview');
        $generate = Yii::$app->request->post('generate');
        $answers = Yii::$app->request->post('answers');
        if ($preview !== null || $generate !== null) {
            if ($generator->validate()) {
                $generator->saveStickyAttributes();
                $files = $generator->generate();
                if ($generate !== null && !empty($answers)) {
                    $params['hasError'] = !$generator->save($files, (array) $answers, $results);
                    $params['results'] = $results;
                } else {
                    $params['files'] = $files;
                    $params['answers'] = $answers;
                }
            }
        }

        return $this->render('view', $params);
    }
}