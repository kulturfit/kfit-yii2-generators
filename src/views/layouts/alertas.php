<style>
    .alert {
        margin: 0 0 10px 0;
        padding: 15px;
        font-size: 14px;
        border-radius: 5px;
    }
    .alert-warning {
        background: #fcf8e3;
        color: #8a6d3b;
        border: 1px solid #faebcc;
    }
</style>
<?php if(!isset(Yii::$app->params['developers'])): ?>
    <div class="alert alert-warning" role="alert"><?= Yii::t('app', "No están definidos los desarrolladores en el archivo de <strong>parámetros</strong>, por favor defina la posición <strong>developers</strong> con el correo electrónico de llave y el nombre del desarrollador como valor.") ?></div>
<?php endif; ?>