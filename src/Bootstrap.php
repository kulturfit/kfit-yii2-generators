<?php

namespace kfit\generators;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 *
 * @package kfit\yii2-generators
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if ($app->hasModule('gii')) {
            if (!isset($app->getModule('gii')->generators['rest'])) {
                $app->getModule('gii')
                    ->generators['rest'] = [
                    'class' => 'kfit\generators\core\rest\Generator',
                ];
            }
            if (!isset($app->getModule('gii')->generators['redis'])) {
                $app->getModule('gii')
                    ->generators['redis'] = [
                    'class' => 'kfit\generators\core\redis\Generator',
                ];
            }
            if (!isset($app->getModule('gii')->generators['tic-crud'])) {
                $app->getModule('gii')
                    ->generators['tic-crud'] = [
                    'class' => 'kfit\generators\core\crud\Generator',
                ];

                unset($app->getModule('gii')->generators['crud']);
            }
            if (!isset($app->getModule('gii')->generators['tic-model'])) {
                $app->getModule('gii')
                    ->generators['tic-model'] = [
                    'class' => 'kfit\generators\core\model\Generator',
                ];
                unset($app->getModule('gii')->generators['model']);
            }
            if (!isset($app->getModule('gii')->generators['migrations'])) {
                $app->getModule('gii')
                    ->generators['migrations'] = [
                    'class' => 'kfit\generators\core\migration\Generator',
                ];
            }
            if (!isset($app->getModule('gii')->generators['graphql'])) {
                $app->getModule('gii')
                    ->generators['graphql'] = [
                    'class' => 'kfit\generators\core\graphql\Generator',
                ];
            }
        }
    }
}
