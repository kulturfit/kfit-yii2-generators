<?php

namespace kfit\generators\core\graphql;

use Yii;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\helpers\StringHelper;

/**
 * @todo documentar código fuente y eliminar metodos innecesarios
 */
class Generator extends \kfit\generators\components\Generator
{

    public $modelClass          = 'app\models\base\\';
    public $controllerClass     = 'app\modules\v1\controllers\\';
    public $baseControllerClass = 'kfit\core\rest\ActiveController';
    public $searchModelClass    = 'app\modules\v1\models\searchs\\';
    public $baseModelClass      = 'app\modules\v1\models\base\\';
    public $fieldCreateDateSync = 'created_at';
    public $fieldUpdateDateSync = 'updated_at';
    public $fieldStatusSync     = 'active';
    public $pathModels          = '@app/models/base';
    public $pathModule          = '@app/modules';
    public $models              = [];
    public $module              = 'v1';
    public $token               = '{{token}}';
    public $useSchemaName       = false;



    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'API Graphql';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Este generador crear un controlador con su respectivo modelo para proveer un servicio Web tipo API graphql.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['baseControllerClass'], 'filter', 'filter' => 'trim'],
                [
                    ['fieldCreateDateSync', 'fieldUpdateDateSync', 'fieldStatusSync', 'baseControllerClass'],
                    'required'
                ],
                [['models', 'module', 'pathModels', 'pathModule'], 'safe'],
                [
                    ['baseControllerClass'],
                    'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'
                ],
                [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'modelClass'          => 'Model Class',
                'controllerClass'     => 'Controller Class',
                'baseControllerClass' => 'Base Controller Class',
                'searchModelClass'    => 'Search Model Class',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(
            parent::hints(),
            [
                'modelClass'          => 'This is the ActiveRecord class associated with the table that CRUD will be built upon.
                You should provide a fully qualified class name, e.g., <code>app\models\Post</code>.',
                'controllerClass'     => 'This is the name of the controller class to be generated. You should
                provide a fully qualified namespaced class (e.g. <code>app\controllers\PostController</code>),
                and class name should be in CamelCase with an uppercase first letter. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
                'baseControllerClass' => 'This is the class that the new CRUD controller class will extend from.
                You should provide a fully qualified class name, e.g., <code>yii\web\Controller</code>.',
                'searchModelClass'    => 'This is the name of the search model class to be generated. You should provide a fully
                qualified namespaced class name, e.g., <code>app\models\PostSearch</code>.',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['resolver.php', 'schema.php'];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['baseControllerClass']);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {

        $files            = [];
        $moduleId = Inflector::camel2id($this->module, '');
        $fullPathModule = $this->joinPaths('\\', $this->pathModule, $moduleId);
        $relations = $this->generateRelations();

        if (empty($this->models)) {
            return [];
        }

        $collectionFile = $this->normalizeClassPath($fullPathModule, 'schema');

        $this->addCodeFile(
            $files,
            $collectionFile,
            'schema.php',
            [
                'moduleId' => Inflector::camel2id($this->module),
                'generator' => $this
            ],
            'js'
        );

        $collectionFile = $this->normalizeClassPath($fullPathModule, 'resolver');

        $this->addCodeFile(
            $files,
            $collectionFile,
            'resolver.php',
            [
                'moduleId' => Inflector::camel2id($this->module),
                'generator' => $this
            ],
            'js'
        );

        return $files;
    }


    /**
     * Returns the message to be displayed when the newly generated code is saved successfully.
     * Child classes may override this method to customize the message.
     * @return string the message to be displayed when the newly generated code is saved successfully.
     */
    public function successMessage()
    {
        return "Generate successfully";
    }

    public function generateRelations(){
        return parent::generateRelations();
    }
}
