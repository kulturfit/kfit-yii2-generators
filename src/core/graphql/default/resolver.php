<?php

use yii\helpers\Inflector;

$moduleId = Inflector::camel2id($generator->module);

$items = [];


$db = Yii::$app->db;

$types = [];

$ignoreAttributes = [
    'active',
    'created_by',
    'created_at',
    'updated_by',
    'updated_at',
];

$queryDef = [];
$mutationDef = [];

foreach ($db->schema->getSchemaNames() as $schema) {
    if ($schema == $db->schema->defaultSchema) { 
        $schema = '';
    }

    foreach ($db->getSchema()->getTableSchemas($schema) as $table) {


    $className = Inflector::camelize($table->name);
    $modelId = Inflector::camel2id($className);

    $primaryKeyTable = [];

    foreach ($table->primaryKey as $primaryKey) {
        $primaryKeyTable[$primaryKey] = $primaryKey;
    }

    if(count($primaryKeyTable) > 0){
    $queryTypesTable = "\t\tview{$className}: (root, args, context, info) => {
\t\t\thttpClient.setContext(context);
\t\t\tconst {[[primaryKey]]} = args
\t\t\tconst {fields, extraFields} = extractFields(info)
\t\t\treturn viewResource('{$modelId}', `\${[[primaryKeyValue]]}`, fields, extraFields)
    },";
}
    $queryTypesTable .= "\n\t\tlist{$className}: (root, args, context, info) => {
\t\t\thttpClient.setContext(context);
\t\t\tconst params = args
\t\t\tconst {fields, extraFields} = extractFields(info, true)
\t\t\treturn listResource('{$modelId}', params, fields, extraFields)
    },";

    $mutationTypesTable = "\t\tcreate{$className}: (root, args, context, info) => {
\t\t\thttpClient.setContext(context);
\t\t\tconst params = args
\t\t\treturn createResource('{$modelId}', params)
    },";

    if(count($primaryKeyTable) > 0){
        $mutationTypesTable .= "\n\t\tdelete{$className}: (root, args, context, info) => {
\t\t\thttpClient.setContext(context);
\t\t\tconst {[[primaryKey]]} = args
\t\t\treturn deleteResource('{$modelId}', `\${[[primaryKeyValue]]}`)
    },";

    $mutationTypesTable .= "\n\t\tupdate{$className}: (root, args, context, info) => {
        \t\t\thttpClient.setContext(context);
        \t\t\tconst {[[primaryKey]]} = args
        \t\t\tconst body = args
        \t\t\treturn updateResource('{$modelId}', `\${[[primaryKeyValue]]}`, body)
            },";
    }


    

    foreach ($table->columns as $column) {

        if (in_array($column->name, $ignoreAttributes)) {
            continue;
        }

        $columnType = "\t{$column->name}:";

        switch ($column->phpType) {

            case 'integer':
                $columnType .= " Int";
                break;
            default:
                $columnType .= " String";
        }

        if(isset($primaryKeyTable[$column->name])){
            $primaryKeyTable[$column->name]="$columnType";
        }
    }

    $queryTypesTable = str_replace('[[primaryKey]]', implode(',', array_keys($primaryKeyTable)), $queryTypesTable);
    $queryTypesTable = str_replace('[[primaryKeyValue]]', implode('},${', array_keys($primaryKeyTable)), $queryTypesTable);
    $mutationTypesTable = str_replace('[[primaryKey]]', implode(',', array_keys($primaryKeyTable)), $mutationTypesTable);
    $mutationTypesTable = str_replace('[[primaryKeyValue]]', implode('},${', array_keys($primaryKeyTable)), $mutationTypesTable);

    $queryDef[] = $queryTypesTable;
    $mutationDef[] = $mutationTypesTable;
}
}


$query = implode("\n\n", $queryDef);
$mutations = implode("\n\n", $mutationDef);
?>

const httpClient = require('../clients/axios-client')
const extractFields = require('../clients/extract-fields');
const queryString = require('query-string');

const resolvers = {
    Query: {
<?php echo print_r($query, true)?>
    },
    Mutation: {
<?php echo print_r($mutations, true)?>
    },
}

const concatFields = (url, type, fields) => {
    if(fields && Array.isArray(fields) && fields.length > 0){
        url = url.concat('&', type,'=',fields.join(','))
    }
    return url
}

const createResource = (resource, params) =>{
    let url = `${resource}/create`
    return httpClient.post(url, params).then(res=>{
        console.log(res.data)
        return res.data;
    }).catch(e=>{
        throw new Error(e.response.data.message)
    });
}

const listResource = (resource, params, fields, extraFields) =>{
    let url = `${resource}?`
    if(params){
        if(params.hasOwnProperty('perPage')){
			params['per-page'] = params.perPage;
		}
        url = url.concat(queryString.stringify(params));
    }
    url = concatFields(url,'fields', fields);
    url = concatFields(url,'expand', extraFields);
    return httpClient.get(url).then(res=>{
        console.log(res.data)
        return res.data;
    }).catch(e=>{
        throw new Error(e.response.data.message)
    });
}

const viewResource = (resource, id, fields, extraFields) =>{
    let url = `${resource}/view?id=${id}`
    url = concatFields(url,'fields', fields);
    url = concatFields(url,'expand', extraFields);
    return httpClient.get(url).then(res=>{
        return res.data;
    }).catch(e=>{
        throw new Error(e.response.data.message)
    });
}
const deleteResource = (resource, id) =>{
    let url = `${resource}/delete?id=${id}`
    return httpClient.put(url).then(res=>{
        return res.data;
    }).catch(e=>{
        throw new Error(e.response.data.message)
    });
}

const updateResource = (resource, id) =>{
    let url = `${resource}/update?id=${id}`
    return httpClient.put(url).then(res=>{
        return res.data;
    }).catch(e=>{
        throw new Error(e.response.data.message)
    });
}

module.exports = resolvers
