<?php

use yii\helpers\Inflector;

$moduleId = Inflector::camel2id($generator->module);

$items = [];


$db = Yii::$app->db;

$types = [];

$ignoreAttributes = [
    'active',
    'created_by',
    'created_at',
    'updated_by',
    'updated_at',
];

$queryTypes = [];
$mutationTypes = [];
$paginationTypes = [];
$relations = $generator->generateRelations();

foreach ($db->schema->getSchemaNames() as $schema) {
    if ($schema == $db->schema->defaultSchema) { 
        $schema = '';
    }

    foreach ($db->getSchema()->getTableSchemas($schema) as $table) {
    
        $className = Inflector::camelize($table->name);
        $modelId = Inflector::camel2id($className);
        $type = "type {$className} {\n[[columns]]\n}";
        $columnsType = [];
        $paginationTypes[] = "type Pagination{$className} {\n\titems: [{$className}]\n\t_meta: MetaPagination\n}";
        $primaryKeyTable = [];
    
        foreach ($table->primaryKey as $primaryKey) {
            $primaryKeyTable[$primaryKey] = $primaryKey;
        }
    
        $queryTypesTable = '';
        
        $mutationTypesTable = "\tcreate{$className}([[columnsRequired]]): {$className}!";
        $mutationTypesTable .= "\n\tupdate{$className}([[columnsRequired]]): {$className}!";
        if(count($primaryKeyTable) > 0){
            $mutationTypesTable .= "\n\tdelete{$className}([[primaryKey]]): DeleteResponse!";
            $queryTypesTable = "\tview{$className}([[primaryKey]]): {$className}";
        }
        
        $queryTypesTable .= "\n\tlist{$className}(sort:String, perPage:Int, page: Int, lastDate:String, [[columns]]): Pagination{$className}";
        $columns = [];
        $columnsRequired = [];
    
        foreach ($table->columns as $column) {
    
            if (in_array($column->name, $ignoreAttributes)) {
                continue;
            }
    
            
    
            $columnType = "\t{$column->name}:";
    
            switch ($column->phpType) {
    
                case 'integer':
                    $columnType .= " Int";
                    break;
                default:
                    $columnType .= " String";
            }
    
            if (!$column->allowNull) {
                $columnsRequired[] = "{$column->name}!";
            } else {
                $columnsRequired[] = "{$column->name}";
            }
    
            $columns[] = $columnType;
    
            $columnsType[] = $columnType;
            if(isset($primaryKeyTable[$column->name])){
                $primaryKeyTable[$column->name]="$columnType";
            }
        }
    
        $queryTypesTable = str_replace('[[columns]]', implode(',', $columns), $queryTypesTable);
        $queryTypesTable = str_replace('[[primaryKey]]', implode(',', $primaryKeyTable), $queryTypesTable);
        $mutationTypesTable = str_replace('[[columnsRequired]]', implode(',', $columns), $mutationTypesTable);
        $mutationTypesTable = str_replace('[[primaryKey]]', implode(',', $primaryKeyTable), $mutationTypesTable);
    
        $tableName = $table->fullName;
        $relationsTable = isset($relations[$tableName]) ? $relations[$tableName] : [];

        // if($tableName == 'actor.actors'){
        //     echo '<pre>';
        //     print_r($relations);
        //     echo '</pre>';
        //     echo '<pre>';
        //     print_r($table->foreignKeys);
        //     echo '</pre>';
        //     exit();
        // }
       

        foreach ($relationsTable as $name => $relation){

            $isHasMany = strpos($relation[0], 'hasMany');
            $foreignId = Inflector::variablize($name);
            if($isHasMany){
                $columnsType[] = " {$foreignId}: [{$relation[1]}]";
            }else{
                $columnsType[] = " {$foreignId}: {$relation[1]}";
            }
            
        }
    
        $types[] = str_replace('[[columns]]', implode("\n", $columnsType), $type);
    
        $queryTypes[] = $queryTypesTable;
        $mutationTypes[] = $mutationTypesTable;
    }
}


$typesClass = implode("\n\n", $types);
$paginationTypes = implode("\n\n", $paginationTypes);

$defaultTypes = "type MetaPagination {\ntotalCount: Int\npageCount: Int\ncurrentPage: Int\nperPage: Int\n}";
$defaultTypes .= "\n\ntype DeleteResponse {\nname: String\nmessage: String\ncode: Int\nstatus: Int\ntype: String}";

$finalDef = "const typeDefs = `[[typesClass]]\n\n[[paginationTypes]]\n\n[[defaultTypes]]\n\ntype Query {\n[[queryTypes]]\n}\n\ntype Mutation {\n[[mutationTypes]]\n}\n`;\n\nmodule.exports = typeDefs";


$finalDef = str_replace('[[typesClass]]', $typesClass, $finalDef);
$finalDef = str_replace('[[queryTypes]]', implode("\n", $queryTypes), $finalDef);
$finalDef = str_replace('[[mutationTypes]]', implode("\n", $mutationTypes), $finalDef);
$finalDef = str_replace('[[defaultTypes]]', $defaultTypes, $finalDef);
$finalDef = str_replace('[[paginationTypes]]', $paginationTypes, $finalDef);

echo print_r($finalDef, true);
