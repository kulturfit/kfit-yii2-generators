<?php

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator kfit\generators\rest\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$modelAlias = $modelClass . 'Model';
    
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

$modelObject = Yii::createObject($generator->modelClass);

$ignoreAttributes = [
    $modelObject::STATUS_COLUMN,
    $modelObject::CREATED_BY_COLUMN,
    $modelObject::CREATED_AT_COLUMN,
    $modelObject::UPDATED_BY_COLUMN,
    $modelObject::UPDATED_AT_COLUMN
];

$fields = [];
foreach ($generator->getColumnNames() as $column) {
    if (!in_array($column, $ignoreAttributes)) {
        $fields[] = $column;
    }
}

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->baseModelClass, '\\')) ?>;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use <?= ltrim($generator->modelClass, '\\') . (" as $modelAlias") ?>;

/**
 * <?= $modelClass ?> 
 * @package <?= StringHelper::dirname(ltrim($generator->baseModelClass, '\\')) ?> 
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright (c) 2020, KulturFit S.A.S.
 * @version 0.0.1
 */
class <?= $modelClass ?> extends <?= $modelAlias ?>

{
    
    /**
     * Campos disponibles
     * ```
     * <?=implode(', ', $fields)?> 
     * ```
     * @return array
     */
    public function fields()
    {
        return [
            <?php 
            foreach ($fields as $field){
                    echo "'{$field}',\n            ";
            }
            echo "\n";
            ?>
        ];
    }

    /**
     * Relaciones disponibles
     * ```
     * <?=implode(', ', $extraFields)?> 
     * ```
     * @return array
     */
    public function extraFields()
    {
        return [
            <?php 
            foreach ($extraFields as $extraField){
                    echo "'{$extraField}',\n            ";
            }
            echo "\n";
            ?>
        ];
    }
}
