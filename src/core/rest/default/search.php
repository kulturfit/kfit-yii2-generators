<?php

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator kfit\generators\rest\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use <?= ltrim($generator->baseModelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;

/**
 * <?= $searchModelClass ?> represents the model behind the search form about `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass ?>

{

    use \kfit\api\rest\traits\SyncSearch;

    /**
     * Permite establecer el número de registros a ser mostrados por pagina
     * @var type
     */
    public $perPage = 20;

    /**
     * Permite identificar los registros excluidos de la consulta
     * @var type
     */
    public $notIn = [];

    /**
     * Permite definir la ultima fecha de sincronización exitosa
     * @var type
     */
    public $lastDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            <?= implode(",\n            ", $rules) ?>,
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (isset($params['per-page']))
        {
            $this->perPage = $params['per-page'];
        }

        if (isset($params['notIn']))
        {
            $params['notIn'] = \yii\helpers\Json::decode($params['notIn']);
            $this->notIn     = is_array($params['notIn']) ? array_values($params['notIn']) : [
                    ];
        }

        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
            /*'sort' => [
                'defaultOrder' => [                    
                    'nombre' => SORT_ASC, 
                ]
            ],*/
        ]);

        $this->setAttributes($params);

        if (!empty($this->lastDate))
        {
            $this->lastDate = str_replace('_', ' ', $this->lastDate);
            $query->andFilterWhere(['>', '<?= $generator->fieldCreateDateSync ?>', $this->lastDate]);
        }

        if ($this->perPage == 0)
        {
            $dataProvider->pagination = false;
        }

        $query->andFilterWhere(['not in', '<?= $generator->getPrimaryKey() ?>', $this->notIn]);

        <?= implode("\n        ", $searchConditions) ?>

        return $dataProvider;
    }
    
}
