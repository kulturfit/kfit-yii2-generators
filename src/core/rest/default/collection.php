<?php

use yii\helpers\Inflector;

$moduleId = Inflector::camel2id($generator->module);

$modelPahtBase = str_replace(
    ['@', '/'],
    ['', '\\'],
    $generator->pathModels
) . '\\';


$items = [];
foreach ($generator->models as $model) {
    $modelId = Inflector::camel2id($model);

    $modelObject = Yii::createObject($modelPahtBase . $model);

    $ignoreAttributes = [
        $modelObject::STATUS_COLUMN,
        $modelObject::CREATED_BY_COLUMN,
        $modelObject::CREATED_AT_COLUMN,
        $modelObject::UPDATED_BY_COLUMN,
        $modelObject::UPDATED_AT_COLUMN
    ];

    $body    = '';
    $bodyAttributes = [];
    $primaryKey = $modelObject->primaryKey();

    if(is_array($primaryKey)){
        $primaryKey = '{'.implode('},{', $primaryKey).'}';
    }else{
        $ignoreAttributes[] = $primaryKey;
        $primaryKey = "{{$primaryKey}}";
    }

    foreach ($modelObject->attributes as $key => $value) {
        if (!in_array($key, $ignoreAttributes)) {
            $bodyAttributes[$key] = "";
        }
    }

    $body = json_encode($bodyAttributes, JSON_PRETTY_PRINT);


    $services = [
        [
            'name'    => 'List',
            "request" => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "GET",
                "header"      => [],
                "body"        => [],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}"
                    ]
                ],
                "description" => ""
            ],
        ],
        [
            'name'    => 'View',
            "request" => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "GET",
                "header"      => [],
                "body"        => [],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/{$primaryKey}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}",
                        "{$primaryKey}"
                    ]
                ],
                "description" => ""
            ],
        ],
        [
            "name"     => "Create",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "POST",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "body"        => [
                    "mode" => "raw",
                    "raw"  => $body
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Update",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "PUT",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "body"        => [
                    "mode" => "raw",
                    "raw"  => $body
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/{$primaryKey}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}",
                        "{$primaryKey}"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Delete",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "DELETE",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/{$primaryKey}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}",
                        "{$primaryKey}"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Sync",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "GET",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/sync?lastDate=" . date('Y-m-d_H:i:s'),
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}",
                        "sync?lastDate=" . date('Y-m-d_H:i:s')
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Create multiple",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "POST",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "body"        => [
                    "mode" => "raw",
                    "raw"  => '{"data":[' . $body . '], "skipErrors":false}'
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/create-all",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}",
                        "create-all"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ]
    ];

    $items[] = [
        "name"        => Inflector::camel2words($model),
        "description" => $generator->getDescriptionModel($modelObject),
        "item"       => $services,
        "_postman_isSubFolder" => true
    ];
}

echo json_encode([
    "info" => [
        'name'        => Inflector::id2camel($moduleId),
        "_postman_id" => "",
        "description" => "",
        "schema"      => "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
    ],
    "item" => [[
        'name'        => Inflector::id2camel($moduleId),
        "description" => "",
        'item'        => $items
    ]]
], JSON_PRETTY_PRINT);
