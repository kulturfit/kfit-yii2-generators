<?php

namespace kfit\generators\core\rest;

use Yii;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\helpers\StringHelper;

/**
 * @todo documentar código fuente y eliminar metodos innecesarios
 */
class Generator extends \kfit\generators\components\Generator
{

    public $modelClass          = 'app\models\base\\';
    public $controllerClass     = 'app\modules\v1\controllers\\';
    public $baseControllerClass = 'kfit\api\rest\ActiveController';
    public $searchModelClass    = 'app\modules\v1\models\searchs\\';
    public $baseModelClass      = 'app\modules\v1\models\base\\';
    public $fieldCreateDateSync = 'created_at';
    public $fieldUpdateDateSync = 'updated_at';
    public $fieldStatusSync     = 'active';
    public $pathModels          = '@app/models/base';
    public $pathModule          = '@app/modules';
    public $models              = [];
    public $module              = 'v1';
    public $token               = '{{token}}';
    public $useSchemaName       = false;



    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'API Rest';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Este generador crear un controlador con su respectivo modelo para proveer un servicio Web tipo API Rest.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['baseControllerClass'], 'filter', 'filter' => 'trim'],
                [
                    ['fieldCreateDateSync', 'fieldUpdateDateSync', 'fieldStatusSync', 'baseControllerClass'],
                    'required'
                ],
                [['models', 'module', 'pathModels', 'pathModule'], 'safe'],
                [
                    ['baseControllerClass'],
                    'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'
                ],
                [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'modelClass'          => 'Model Class',
                'controllerClass'     => 'Controller Class',
                'baseControllerClass' => 'Base Controller Class',
                'searchModelClass'    => 'Search Model Class',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(
            parent::hints(),
            [
                'modelClass'          => 'This is the ActiveRecord class associated with the table that CRUD will be built upon.
                You should provide a fully qualified class name, e.g., <code>app\models\Post</code>.',
                'controllerClass'     => 'This is the name of the controller class to be generated. You should
                provide a fully qualified namespaced class (e.g. <code>app\controllers\PostController</code>),
                and class name should be in CamelCase with an uppercase first letter. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
                'baseControllerClass' => 'This is the class that the new CRUD controller class will extend from.
                You should provide a fully qualified class name, e.g., <code>yii\web\Controller</code>.',
                'searchModelClass'    => 'This is the name of the search model class to be generated. You should provide a fully
                qualified namespaced class name, e.g., <code>app\models\PostSearch</code>.',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['controller.php'];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['baseControllerClass']);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {

        $files            = [];
        $moduleId = Inflector::camel2id($this->module, '');
        $fullPathModule = $this->joinPaths('\\', $this->pathModule, $moduleId);
        $pathControllers  =  $this->joinPaths('\\', $fullPathModule, 'controllers');
        $pathSearchModels = $this->joinPaths('\\', $fullPathModule, 'models\\searchs');
        $pathBaseModels = $this->joinPaths('\\', $fullPathModule, 'models\\base');
        $relations = $this->generateRelations();

        if (empty($this->models)) {
            return [];
        }

        foreach ($this->models as $model) {
            /* Nombres de las clases */
            $controllerName = Inflector::pluralize(Inflector::camelize(StringHelper::basename($model))) . 'Controller';
            $modelName      = Inflector::camelize(StringHelper::basename($model));


            /* Path de las clases */
            $this->modelClass = $this->normalizeClassPath($this->pathModels, $modelName);
            $this->controllerClass = $this->normalizeClassPath($pathControllers, $controllerName);
            $this->searchModelClass = $this->normalizeClassPath($pathSearchModels, $modelName);
            $this->baseModelClass = $this->normalizeClassPath($pathBaseModels, $modelName);

            $tableName = $this->modelClass::tableName();

            $relationNames = array_keys(isset($relations[$tableName]) ? $relations[$tableName] : []);
            $extraFields = [];

            foreach ($relationNames as $relationName) {
                $extraFields[] = Inflector::variablize($relationName);
            }

            $this->addCodeFile($files, $this->controllerClass, 'controller.php');
            $this->addCodeFile($files, $this->baseModelClass, 'model.php', [
                'extraFields' => $extraFields
            ]);
            $this->addCodeFile($files, $this->searchModelClass, 'search.php');
        }

        $collectionFile = $this->normalizeClassPath($fullPathModule, 'collection');

        $this->addCodeFile(
            $files,
            $collectionFile,
            'collection.php',
            [
                'moduleId' => Inflector::camel2id($this->module),
                'generator' => $this
            ],
            'json'
        );

        return $files;
    }


    /**
     * Returns the message to be displayed when the newly generated code is saved successfully.
     * Child classes may override this method to customize the message.
     * @return string the message to be displayed when the newly generated code is saved successfully.
     */
    public function successMessage()
    {

        $moduleId      = Inflector::camel2id($this->module);
        $controladores = "";
        foreach ($this->models as $model) {
            $modelName     = Inflector::pluralize(Inflector::camel2Id($model));
            $controladores .= "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'{$moduleId}/{$modelName}',";
        }
        $controladores = trim($controladores, ',');
        $resultado     = "[<br>
        &nbsp;&nbsp;&nbsp;&nbsp;'class' => 'yii\\rest\\UrlRule',<br>
        &nbsp;&nbsp;&nbsp;&nbsp;'controller' => [
        {$controladores}<br>
        &nbsp;&nbsp;&nbsp;&nbsp;],<br>
        &nbsp;&nbsp;&nbsp;&nbsp;'pluralize' => false<br>
        ]";
        return "Rules<pre style=\"color: black;\">" . $resultado . "</pre>";
    }
}
