<?php

namespace kfit\generators\core\model;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Schema;
use yii\db\TableSchema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\base\NotSupportedException;
use yii\db\Query;

/**
 * Este generador genera uno o varios modelos basados en ActiveRecord de las tablase especificadas.
 * @package kfit
 * @subpackage generators
 * @category Generators
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class Generator extends \yii\gii\Generator
{
    use \kfit\generators\components\traits\TraitConfigurations;

    const RELATIONS_NONE        = 'none';
    const RELATIONS_ALL         = 'all';
    const RELATIONS_ALL_INVERSE = 'all-inverse';

    public $db                         = 'db';
    public $ns                         = 'app\models\base\\';
    public $tableName;
    public $tableNames;
    public $modelClass;
    public $baseClass                  = 'kfit\core\base\Model';
    public $developer;
    public $copyright                  = 'KulturFit S.A.S.';
    public $since                      = '1.0.0';
    public $generateRelations          = self::RELATIONS_ALL;
    public $generateLabelsFromComments = false;
    public $useTablePrefix             = false;
    public $useSchemaName              = true;
    public $generateQuery              = false;
    public $generateAppModel           = false;
    public $queryNs                    = 'app\models\base\\';
    public $appNs                      = 'app\models\app';
    public $queryClass;
    public $queryBaseClass             = 'yii\db\ActiveQuery';
    public $enableI18N                 = true;
    public $ignoreColumnsRules         = [
        'activo',
        'creado_por',
        'fecha_creacion',
        'modificado_por',
        'fecha_modificacion',
        'active',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Modelos';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Este generador crea clases ActiveRecord para tablas específicas de la base de datos.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [[
                    'db', 'ns', 'tableName', 'modelClass', 'baseClass', 'queryNs', 'queryClass',
                    'queryBaseClass'
                ], 'filter', 'filter' => 'trim'],
                [['ns', 'queryNs'], 'filter', 'filter' => function ($value) {
                    return trim($value, '\\');
                }],
                [
                    ['db', 'ns', 'baseClass', 'developer', 'tableNames', 'queryNs', 'queryBaseClass'],
                    'required'
                ],
                [
                    ['db', 'modelClass', 'queryClass'], 'match', 'pattern' => '/^\w+$/',
                    'message' => 'Only word characters are allowed.'
                ],
                [
                    ['ns', 'baseClass', 'queryNs', 'queryBaseClass'], 'match', 'pattern' => '/^[\w\\\\]+$/',
                    'message' => 'Only word characters and backslashes are allowed.'
                ],
                [['tableName'], 'match', 'pattern' => '/^([\w ]+\.)?([\w\* ]+)$/', 'message' => 'Only word characters, and optionally spaces, an asterisk and/or a dot are allowed.'],
                [['db'], 'validateDb'],
                [['ns', 'queryNs'], 'validateNamespace'],
                [['baseClass'], 'validateClass', 'params' => ['extends' => ActiveRecord::class]],
                [['queryBaseClass'], 'validateClass', 'params' => ['extends' => ActiveQuery::class]],
                [['generateRelations'], 'in', 'range' => [
                    self::RELATIONS_NONE, self::RELATIONS_ALL,
                    self::RELATIONS_ALL_INVERSE
                ]],
                [
                    ['generateLabelsFromComments', 'useTablePrefix', 'useSchemaName', 'generateQuery'],
                    'boolean'
                ],
                [['enableI18N'], 'boolean'],
                [['messageCategory'], 'validateMessageCategory', 'skipOnEmpty' => false],
                ['generateAppModel', 'safe']
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'ns'                         => 'Namespace',
                'db'                         => 'Id de conexión a Base de Datos',
                'tableName'                  => 'Nombre de Tabla',
                'tableNames'                 => 'Nombres de las Tablas',
                'modelClass'                 => 'Model Class',
                'baseClass'                  => 'Class Base',
                'developer'                  => 'Desarrollador Autor',
                'useTablePrefix'             => 'Usar prefijo de tablas',
                'generateRelations'          => 'Generar Relaciones',
                'generateLabelsFromComments' => 'Generar Labels desde comentarios de Base de Datos',
                'generateQuery'              => 'Generar ActiveQuery',
                'queryNs'                    => 'ActiveQuery Namespace',
                'queryClass'                 => 'ActiveQuery Class',
                'queryBaseClass'             => 'ActiveQuery Class Base',
                'enableI18N'                 => 'Habilitar I18N',
                'messageCategory'            => 'Categoría de Mensajes',
                'useSchemaName'              => 'Use Schema Name',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(
            parent::hints(),
            [
                'ns'                         => 'Este es el namespace de la clase ActiveRecord para ser generado, e.g., <code>app\models\base</code>',
                'db'                         => 'Este es el ID del componente BD.',
                'tableNames'                 => 'Nombres de las tabas para las nuevas clases ActiveRecord',
                'tableName'                  => 'Este es el nombre de la tabla en la BD para la nueva clase ActiveRecord e.g. <code>post</code>.
                El nombre de la tabla puede consistir en la parte esquema de BD si es necesario, e.g. <code>public.post</code>.
                El nombre de la tabla puede terminar con asterisco para que coincida con múltiples nombres de tabla, e.g. <code>tbl_*</code>
                coincidirá con tablas cuyo nombre empieze con <code>tbl_</code>. En este caso, múltiples clases ActiveRecord
                van a ser generadas, una para cada nombre de la tabla; y los nombres de las clases se generan a partir de
                los caracteres coincidentes. Por ejemplo, tabla <code>tbl_post</code> generará la clase <code>Post</code>.',
                'modelClass'                 => 'Este es el nombre de la clase ActiveRecord que va a ser generada. El nombre de la clase no debe contener
                la parte del namespace, esta es espeficada en "Namespace". No es necesario especificar el nombre de la clase
                si "Nombre de Tabla" finaliza con asterisco, en cuyo caso múltiples clases ActiveRecord van a ser generadas.',
                'baseClass'                  => 'Esta es la clase base de la nueva clase ActiveRecord. Debe ser un nombre de clase con namespace completo, e.g. <code>app\components\Model</code>.',
                'generateRelations'          => 'Esto indica si el generador debe generar relaciones basadas en
                restricciones de claves foráneas que detecte en la base de datos. Tenga en cuenta que si su base de datos contiene también muchas tablas,
                es posible que desee desactivar esta opción para acelerar el proceso de generación de código.',
                'developer'                  => 'Los datos del desarrollador que aparecerá en el tag @autor de la clase',
                'generateLabelsFromComments' => 'Esto indica si el generador debe generar labels
                mediante el uso de los comentarios de las columnas de la BD correspondientes.',
                'useTablePrefix'             => 'Esto indica si el nombre de la tabla devuelta por la clase ActiveRecord generada
                debe considerar el <code>tablePrefix</code> configurado en la conexión de BD. Por ejemplo, si el
                nombre de la tabla es <code>tbl_post</code> y <code>tablePrefix=tbl_</code>, la clase ActiveRecord
                devolverá el nombre de tabla como <code>{{%post}}</code>.',
                'useSchemaName'              => 'Esto indica si se debe incluir el nombre de esquema en la clase ActiveRecord
                cuando es autogenerada. Sólo el esquema predeterminado no se utilizaría.',
                'generateQuery'              => 'Esto indica si se debe generar ActiveQuery para la clase ActiveRecord.',
                'queryNs'                    => 'Este es el namespace de la clase ActiveQuery que va a ser generada, e.g., <code>app\models</code>',
                'queryClass'                 => 'Este es el nombre de la clase ActiveQuery que va a ser generada. El nombre de la clase no debe contener
                la parte del namespace, esta es espeficada en "ActiveQuery Namespace". No es necesario especificar el nombre de la clase
                si "Nombre de Tabla" finaliza con asterisco, en cuyo caso múltiples clasesActiveQuery van a ser generadas.',
                'queryBaseClass'             => 'Esta es la clase base de la nueva clase ActiveQuery . It should be a fully qualified namespaced class name.',
                'enableI18N'                 => 'Esto indica si el generador debe generar strings utilizando el método <code>Yii::t()</code>.
                Coloca en <code>true</code> si desesas manejar traducciones en tu aplicación.',
                'messageCategory'            => 'Esta es la categoria usada por <code>Yii::t()</code> en caso de habilitar I18N..',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function autoCompleteData()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return [
                'tableName' => function () use ($db) {
                    return $db->getSchema()->getTableNames();
                },
            ];
        } else {
            return [];
        }
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        // @todo make 'query.php' to be required before 2.1 release
        return ['model.php'/* , 'query.php' */];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(
            parent::stickyAttributes(),
            [
                'ns', 'db', 'baseClass', 'generateRelations', 'generateLabelsFromComments',
                'queryNs', 'queryBaseClass'
            ]
        );
    }

    /**
     * Returns the `tablePrefix` property of the DB connection as specified
     *
     * @return string
     * @since 2.0.5
     * @see getDbConnection
     */
    public function getTablePrefix()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return $db->tablePrefix;
        } else {
            return '';
        }
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files     = [];
        $relations = $this->generateRelations();
        $db        = $this->getDbConnection();
        /* Obtiene los nombres de las tablas seleccionadas */
        foreach ($this->tableNames as $tableName) {
            // model :
            $modelClassName = $this->generateClassName($tableName);
            $queryClassName = ($this->generateQuery) ? $this->generateQueryClassName($modelClassName) : false;
            $tableSchema    = $db->getTableSchema($tableName);

            $params  = [
                'tableName'      => $tableName,
                'className'      => $modelClassName,
                'queryClassName' => $queryClassName,
                'tableSchema'    => $tableSchema,
                'labels'         => $this->generateLabels($tableSchema),
                'rules'          => $this->generateRules($tableSchema),
                'relations'      => isset($relations[$tableName]) ? $relations[$tableName] : [],
            ];
            $files[] = new CodeFile(
                Yii::getAlias('@' . str_replace('\\', '/', $this->ns)) . '/' . $modelClassName . '.php',
                $this->render(
                    'model.php',
                    $params
                )
            );

            // appModel :

            if ($this->generateAppModel) {
                $params['modelClassName'] = $modelClassName;
                $files[]                  = new CodeFile(
                    Yii::getAlias('@' . str_replace(
                        '\\',
                        '/',
                        $this->appNs
                    )) . '/' . $modelClassName . '.php',
                    $this->render(
                        'app.php',
                        $params
                    )
                );
            }


            // query :
            if ($queryClassName) {
                $params['className']      = $queryClassName;
                $params['modelClassName'] = $modelClassName;
                $files[]                  = new CodeFile(
                    Yii::getAlias('@' . str_replace(
                        '\\',
                        '/',
                        $this->queryNs
                    )) . '/' . $queryClassName . '.php',
                    $this->render(
                        'query.php',
                        $params
                    )
                );
            }
        }

        return $files;
    }

    /**
     * Generates the attribute labels for the specified table.
     * @param \yii\db\TableSchema $table the table schema
     * @return array the generated attribute labels (name => label)
     */
    public function generateLabels($table)
    {
        $labels = [];
        foreach ($table->columns as $column) {
            if ($this->generateLabelsFromComments && !empty($column->comment)) {
                $labels[$column->name] = $column->comment;
            } elseif ($column->isPrimaryKey) {
                $labels[$column->name] = 'Código';
            } else {
                $label = Inflector::camel2words($column->name);
                if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                    $label = substr($label, 0, -3);
                }
                $labels[$column->name] = $label;
            }
        }

        return $labels;
    }

    /**
     * Generates validation rules for the specified table.
     * @param \yii\db\TableSchema $table the table schema
     * @return array the generated validation rules
     */
    public function generateRules($table)
    {
        $types   = [];
        $lengths = [];
        foreach ($table->columns as $column) {
            if ($column->autoIncrement || in_array(
                $column->name,
                $this->ignoreColumnsRules
            )) {
                continue;
            }
            if (!$column->allowNull && $column->defaultValue === null) {
                $types['required'][] = $column->name;
            }
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                    $types['integer'][] = $column->name;
                    break;
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case 'double': // Schema::TYPE_DOUBLE, which is available since Yii 2.0.3
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $types['number'][]  = $column->name;
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $types['safe'][]    = $column->name;
                    break;
                default: // strings
                    if ($column->size > 0) {
                        $lengths[$column->size][] = $column->name;
                    } else {
                        $types['string'][] = $column->name;
                    }
            }
        }
        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }
        foreach ($lengths as $length => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], 'string', 'max' => $length]";
        }

        $db = $this->getDbConnection();

        // Unique indexes rules
        try {
            $uniqueIndexes = $db->getSchema()->findUniqueIndexes($table);
            foreach ($uniqueIndexes as $uniqueColumns) {
                // Avoid validating auto incremental columns
                if (!$this->isColumnAutoIncremental($table, $uniqueColumns)) {
                    $attributesCount = count($uniqueColumns);

                    if ($attributesCount === 1) {
                        $rules[] = "[['" . $uniqueColumns[0] . "'], 'unique']";
                    } elseif ($attributesCount > 1) {
                        $labels      = array_intersect_key(
                            $this->generateLabels($table),
                            array_flip($uniqueColumns)
                        );
                        $lastLabel   = array_pop($labels);
                        $columnsList = implode("', '", $uniqueColumns);
                        $rules[]     = "[['$columnsList'], 'unique', 'targetAttribute' => ['$columnsList'], 'message' => 'The combination of " . implode(
                            ', ',
                            $labels
                        ) . " and $lastLabel has already been taken.']";
                    }
                }
            }
        } catch (NotSupportedException $e) {
            // doesn't support unique indexes information...do nothing
        }

        // Exist rules for foreign keys
        foreach ($table->foreignKeys as $refs) {
            $refTable       = $refs[0];
            $refTableSchema = $db->getTableSchema($refTable);
            if ($refTableSchema === null) {
                // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                continue;
            }
            $refClassName     = $this->generateClassName($refTable);
            unset($refs[0]);
            $attributes       = implode("', '", array_keys($refs));
            $targetAttributes = [];
            foreach ($refs as $key => $value) {
                $targetAttributes[] = "'$key' => '$value'";
            }
            $targetAttributes = implode(', ', $targetAttributes);
            $rules[]          = "[['$attributes'], 'exist', 'skipOnError' => true, 'targetClass' => Yii::\$container->get(\\{$this->appNs}\\{$refClassName}::class), 'targetAttribute' => [$targetAttributes], 'when' => function (\$model) {
                    return !(\$model instanceof \\kfit\\core\\redis\\Model);
                }]";
        }

        return $rules;
    }

    /**
     * Generates relations using a junction table by adding an extra viaTable().
     * @param \yii\db\TableSchema the table being checked
     * @param array $fks obtained from the checkJunctionTable() method
     * @param array $relations
     * @return array modified $relations
     */
    private function generateManyManyRelations($table, $fks, $relations)
    {
        $db = $this->getDbConnection();

        foreach ($fks as $pair) {
            list($firstKey, $secondKey) = $pair;
            $table0       = $firstKey[0];
            $table1       = $secondKey[0];
            unset($firstKey[0], $secondKey[0]);
            $className0   = $this->generateClassName($table0);
            $className1   = $this->generateClassName($table1);
            $table0Schema = $db->getTableSchema($table0);
            $table1Schema = $db->getTableSchema($table1);

            $link                                              = $this->generateRelationLink(array_flip($secondKey));
            $viaLink                                           = $this->generateRelationLink($firstKey);
            $relationName                                      = $this->generateRelationName(
                $relations,
                $table0Schema,
                key($secondKey),
                true
            );
            $relations[$table0Schema->fullName][$relationName] = [
                "\$query = \$this->hasMany(Yii::\$container->get(\\{$this->appNs}\\{$className1}::class), $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', $viaLink);\n
                \$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE]);\n
                \n
                return \$query;",
                $className1,
                true,
            ];

            $link                                              = $this->generateRelationLink(array_flip($firstKey));
            $viaLink                                           = $this->generateRelationLink($secondKey);
            $relationName                                      = $this->generateRelationName(
                $relations,
                $table1Schema,
                key($firstKey),
                true
            );
            $relations[$table1Schema->fullName][$relationName] = [
                "\$query = \$this->hasMany(Yii::\$container->get(\\{$this->appNs}\\{$className0}::class), $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', $viaLink);\n
                \$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE]);\n
                \n
                return \$query;",
                $className0,
                true,
            ];
        }

        return $relations;
    }

    /**
     * @return string[] all db schema names or an array with a single empty string
     * @throws NotSupportedException
     * @since 2.0.5
     */
    protected function getSchemaNames()
    {
        $db     = $this->getDbConnection();
        $schema = $db->getSchema();
        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }
        return $schemaNames;
    }

    /**
     * @return array the generated relation declarations
     */
    protected function generateRelations()
    {
        if ($this->generateRelations === self::RELATIONS_NONE) {
            return [];
        }

        $db = $this->getDbConnection();

        $relations = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $db->getTableSchema($refTable);
                    if ($refTableSchema === null) {
                        // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                        continue;
                    }
                    unset($refs[0]);
                    $fks          = array_keys($refs);
                    $refClassName = $this->generateClassName($refTable);

                    // Add relation for this table
                    $link                                       = $this->generateRelationLink(array_flip($refs));
                    $relationName                               = $this->generateRelationName(
                        $relations,
                        $table,
                        $fks[0],
                        false
                    );
                    $relations[$table->fullName][$relationName] = [
                        "return \$this->hasOne(Yii::\$container->get(\\{$this->appNs}\\{$refClassName}::class), $link)->cache(3);",
                        $refClassName,
                        false,
                    ];

                    // Add relation for the referenced table
                    $hasMany                                             = $this->isHasManyRelation(
                        $table,
                        $fks
                    );
                    $link                                                = $this->generateRelationLink($refs);
                    $relationName                                        = $this->generateRelationName(
                        $relations,
                        $refTableSchema,
                        $className,
                        $hasMany
                    );
                    $relations[$refTableSchema->fullName][$relationName] = [
                        $hasMany ? "\$query = \$this->hasMany(Yii::\$container->get(\\{$this->appNs}\\{$className}::class), $link);\n\t\t\$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);\n\t\treturn \$query;" : "return \$this->hasOne(Yii::\$container->get(\\{$this->appNs}\\{$className}::class), $link)->cache(3);",
                        $className,
                        $hasMany,
                    ];
                }

                if (($junctionFks = $this->checkJunctionTable($table)) === false) {
                    continue;
                }

                $relations = $this->generateManyManyRelations(
                    $table,
                    $junctionFks,
                    $relations
                );
            }
        }

        if ($this->generateRelations === self::RELATIONS_ALL_INVERSE) {
            return $this->addInverseRelations($relations);
        }
        return $relations;
    }

    /**
     * Adds inverse relations
     *
     * @param array $relations relation declarations
     * @return array relation declarations extended with inverse relation names
     * @since 2.0.5
     */
    protected function addInverseRelations($relations)
    {
        $relationNames = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($this->getDbConnection()->getSchema()->getTableSchemas($schemaName) as
                $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $this->getDbConnection()->getTableSchema($refTable);
                    unset($refs[0]);
                    $fks            = array_keys($refs);

                    $leftRelationName                                             = $this->generateRelationName(
                        $relationNames,
                        $table,
                        $fks[0],
                        false
                    );
                    $relationNames[$table->fullName][$leftRelationName]           = true;
                    $hasMany                                                      = $this->isHasManyRelation(
                        $table,
                        $fks
                    );
                    $rightRelationName                                            = $this->generateRelationName(
                        $relationNames,
                        $refTableSchema,
                        $className,
                        $hasMany
                    );
                    $relationNames[$refTableSchema->fullName][$rightRelationName] = true;

                    $relations[$table->fullName][$leftRelationName][0]           = rtrim(
                        $relations[$table->fullName][$leftRelationName][0],
                        ';'
                    )
                        . "->inverseOf('" . lcfirst($rightRelationName) . "');";
                    $relations[$refTableSchema->fullName][$rightRelationName][0] = rtrim(
                        $relations[$refTableSchema->fullName][$rightRelationName][0],
                        ';'
                    )
                        . "->inverseOf('" . lcfirst($leftRelationName) . "');";
                }
            }
        }
        return $relations;
    }

    /**
     * Determines if relation is of has many type
     *
     * @param TableSchema $table
     * @param array $fks
     * @return boolean
     * @since 2.0.5
     */
    protected function isHasManyRelation($table, $fks)
    {
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDbConnection()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        foreach ($uniqueKeys as $uniqueKey) {
            if (count(array_diff(
                array_merge($uniqueKey, $fks),
                array_intersect($uniqueKey, $fks)
            )) === 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generates the link parameter to be used in generating the relation declaration.
     * @param array $refs reference constraint
     * @return string the generated link parameter.
     */
    protected function generateRelationLink($refs)
    {
        $pairs = [];
        foreach ($refs as $a => $b) {
            $pairs[] = "'$a' => '$b'";
        }

        return '[' . implode(', ', $pairs) . ']';
    }

    /**
     * Checks if the given table is a junction table, that is it has at least one pair of unique foreign keys.
     * @param \yii\db\TableSchema the table being checked
     * @return array|boolean all unique foreign key pairs if the table is a junction table,
     * or false if the table is not a junction table.
     */
    protected function checkJunctionTable($table)
    {
        if (count($table->foreignKeys) < 2) {
            return false;
        }
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDbConnection()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        $result      = [];
        // find all foreign key pairs that have all columns in an unique constraint
        $foreignKeys = array_values($table->foreignKeys);
        for ($i = 0; $i < count($foreignKeys); $i++) {
            $firstColumns = $foreignKeys[$i];
            unset($firstColumns[0]);

            for ($j = $i + 1; $j < count($foreignKeys); $j++) {
                $secondColumns = $foreignKeys[$j];
                unset($secondColumns[0]);

                $fks = array_merge(
                    array_keys($firstColumns),
                    array_keys($secondColumns)
                );
                foreach ($uniqueKeys as $uniqueKey) {
                    if (count(array_diff(
                        array_merge($uniqueKey, $fks),
                        array_intersect(
                            $uniqueKey,
                            $fks
                        )
                    )) === 0) {
                        // save the foreign key pair
                        $result[] = [$foreignKeys[$i], $foreignKeys[$j]];
                        break;
                    }
                }
            }
        }
        return empty($result) ? false : $result;
    }

    /**
     * Generate a relation name for the specified table and a base name.
     * @param array $relations the relations being generated currently.
     * @param \yii\db\TableSchema $table the table schema
     * @param string $key a base name that the relation name may be generated from
     * @param boolean $multiple whether this is a has-many relation
     * @return string the relation name
     */
    protected function generateRelationName($relations, $table, $key, $multiple)
    {
        if (!empty($key) && substr_compare($key, 'id', -2, 2, true) === 0 && strcasecmp(
            $key,
            'id'
        )) {
            $key = rtrim(substr($key, 0, -2), '_');
        }
        if ($multiple) {
            $key = Inflector::pluralize($key);
        }
        $name    = $rawName = Inflector::id2camel($key, '_');
        $i       = 0;
        while (isset($table->columns[lcfirst($name)])) {
            $name = $rawName . ($i++);
        }
        while (isset($relations[$table->fullName][$name])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * Validates the [[db]] attribute.
     */
    public function validateDb()
    {
        if (!Yii::$app->has($this->db)) {
            $this->addError(
                'db',
                'There is no application component named "db".'
            );
        } elseif (!Yii::$app->get($this->db) instanceof Connection) {
            $this->addError(
                'db',
                'The "db" application component must be a DB connection instance.'
            );
        }
    }

    /**
     * Validates the namespace.
     *
     * @param string $attribute Namespace variable.
     */
    public function validateNamespace($attribute)
    {
        $value = $this->$attribute;
        $value = ltrim($value, '\\');
        $path  = Yii::getAlias('@' . str_replace('\\', '/', $value), false);
        if ($path === false) {
            $this->addError(
                $attribute,
                'El Namespace debe estar asociado a un directorio existente.'
            );
        }
    }

    /**
     * Validates the [[modelClass]] attribute.
     */
    public function validateModelClass()
    {
        if ($this->isReservedKeyword($this->modelClass)) {
            $this->addError(
                'modelClass',
                'Class name cannot be a reserved PHP keyword.'
            );
        }
        if ((empty($this->tableName) || substr_compare(
            $this->tableName,
            '*',
            -1,
            1
        )) && $this->modelClass == '') {
            $this->addError(
                'modelClass',
                'Model Class cannot be blank if table name does not end with asterisk.'
            );
        }
    }

    /**
     * Validates the [[tableName]] attribute.
     */
    public function validateTableName()
    {
        if (strpos($this->tableName, '*') !== false && substr_compare(
            $this->tableName,
            '*',
            -1,
            1
        )) {
            $this->addError(
                'tableName',
                'EL Asterisco solo es permitido en el último caracter.'
            );

            return;
        }
        $tables = $this->getTableNames();
        if (empty($tables)) {
            $this->addError(
                'tableName',
                "La Tabla '{$this->tableName}' no existe."
            );
        } else {
            foreach ($tables as $table) {
                $class = $this->generateClassName($table);
                if ($this->isReservedKeyword($class)) {
                    $this->addError(
                        'tableName',
                        "La Tabla '$table' generará una clase que es una palabra reservada PHP."
                    );
                    break;
                }
            }
        }
    }

    protected $classNames;

    /**
     * @return array the table names that match the pattern specified by [[tableName]].
     */
    public function getTableNames()
    {
        $db = $this->getDbConnection();
        if ($db === null) {
            return [];
        }
        $tableNames = [];
        $schema     = '';

        if ($db->driverName == 'pgsql') {
            foreach ($db->schema->getSchemaNames() as $schema) {
                if ($schema == $db->schema->defaultSchema) { 
                    $schema = '';
                }

                foreach ($db->schema->getTableNames($schema) as $table) {
                    $tableNames[$schema === '' ? $table : ($schema . '.' . $table)] = $schema === '' ? $table : ($schema . '.' . $table);
                }
                
            }
        } elseif ($this->getDbConnection()->driverName == 'mysql') {
            foreach ($db->schema->getTableNames($schema) as $table) {
                $tableNames[$schema === '' ? $table : ($schema . '.' . $table)] = $schema === '' ? $table : ($schema . '.' . $table);
            }
        }

        return $tableNames;
    }

    /**
     * Método encargado de entregar el comentario de una tabla de la base de datos
     *
     * @param string $tableName Nombre de la tabla (con schema si es necesario)
     * @return string
     */
    public function getComment($tableName)
    {
        $db      = $this->getDbConnection();
        $comment = '';

        if ($db->driverName == 'pgsql') {
            $command = $db->createCommand("SELECT obj_description('{$tableName}'::regclass, 'pg_class') AS comment");
            $comment = $command->queryOne()['comment'];
        } elseif ($this->getDbConnection()->driverName == 'mysql') {
            $query = new Query;
            $query->select('table_comment as comment');
            $query->from('INFORMATION_SCHEMA.TABLES');

            if (strpos($tableName, '.') !== false) {
                $tableNaname = explode('.', $tableName);
                $schema      = $tableNaname[0];
                $table       = $tableNaname[1];
                $query->andWhere(['table_schema' => $schema]);
                $query->andWhere(['table_name' => $table]);
            } else {
                $query->andWhere(['table_name' => $tableName]);
            }
            $comment = $query->one()['comment'];
        }


        return $comment;
    }

    /**
     * Generates the table name by considering table prefix.
     * If [[useTablePrefix]] is false, the table name will be returned without change.
     * @param string $tableName the table name (which may contain schema prefix)
     * @return string the generated table name
     */
    public function generateTableName($tableName)
    {
        if (!$this->useTablePrefix) {
            return $tableName;
        }

        $db = $this->getDbConnection();
        if (preg_match("/^{$db->tablePrefix}(.*?)$/", $tableName, $matches)) {
            $tableName = '{{%' . $matches[1] . '}}';
        } elseif (preg_match("/^(.*?){$db->tablePrefix}$/", $tableName, $matches)) {
            $tableName = '{{' . $matches[1] . '%}}';
        }
        return $tableName;
    }

    /**
     * Generates a class name from the specified table name.
     * @param string $tableName the table name (which may contain schema prefix)
     * @param boolean $useSchemaName should schema name be included in the class name, if present
     * @return string the generated class name
     */
    protected function generateClassName($tableName, $useSchemaName = null)
    {
        if (isset($this->classNames[$tableName])) {
            return $this->classNames[$tableName];
        }

        $schemaName    = '';
        $fullTableName = $tableName;
        if (($pos           = strrpos($tableName, '.')) !== false) {
            if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                $schemaName = substr($tableName, 0, $pos) . '_';
            }
            $tableName = substr($tableName, $pos + 1);
        }

        $db         = $this->getDbConnection();
        $patterns   = [];
        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        $patterns[] = "/^(.*?){$db->tablePrefix}$/";
        if (strpos($this->tableName, '*') !== false) {
            $pattern = $this->tableName;
            if (($pos     = strrpos($pattern, '.')) !== false) {
                $pattern = substr($pattern, $pos + 1);
            }
            $patterns[] = '/^' . str_replace('*', '(\w+)', $pattern) . '$/';
        }
        $className = $tableName;
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $tableName, $matches)) {
                $className = $matches[1];
                break;
            }
        }

        return $this->classNames[$fullTableName] = Inflector::id2camel(
            $schemaName . $className,
            '_'
        );
    }

    /**
     * Generates a query class name from the specified model class name.
     * @param string $modelClassName model class name
     * @return string generated class name
     */
    protected function generateQueryClassName($modelClassName)
    {
        $queryClassName = $this->queryClass;
        if (empty($queryClassName) || strpos($this->tableName, '*') !== false) {
            $queryClassName = $modelClassName . 'Query';
        }
        return $queryClassName;
    }

    /**
     * @return Connection the DB connection as specified by [[db]].
     */
    protected function getDbConnection()
    {
        return Yii::$app->get($this->db, false);
    }

    /**
     * Checks if any of the specified columns is auto incremental.
     * @param \yii\db\TableSchema $table the table schema
     * @param array $columns columns to check for autoIncrement property
     * @return boolean whether any of the specified columns is auto incremental.
     */
    protected function isColumnAutoIncremental($table, $columns)
    {
        foreach ($columns as $column) {
            if (isset($table->columns[$column]) && $table->columns[$column]->autoIncrement) {
                return true;
            }
        }

        return false;
    }

    /**
     * Generates a string depending on enableI18N property
     *
     * @param string $string the text be generated
     * @param array $placeholders the placeholders to use by `Yii::t()`
     * @return string
     */
    public function generateString($string = '', $placeholders = [])
    {
        $string = ucfirst(strtolower(addslashes($string)));
        if ($this->enableI18N) {
            // If there are placeholders, use them
            if (!empty($placeholders)) {
                $ph = ', ' . VarDumper::export($placeholders);
            } else {
                $ph = '';
            }
            $str = "Yii::t('" . $this->messageCategory . "', '" . $string . "'" . $ph . ")";
        } else {
            // No I18N, replace placeholders by real words, if any
            if (!empty($placeholders)) {
                $phKeys = array_map(function ($word) {
                    return '{' . $word . '}';
                }, array_keys($placeholders));
                $phValues = array_values($placeholders);
                $str = "'" . str_replace($phKeys, $phValues, $string) . "'";
            } else {
                // No placeholders, just the given string
                $str = "'" . $string . "'";
            }
        }
        return $str;
    }
}
