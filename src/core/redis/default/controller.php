<?php

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator kfit\generators\rest\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use <?= ltrim($generator->baseControllerClass, '\\') ?>;

/**
* <?= $controllerClass ?> Clase encargada de presentar y manipular la información del modelo <?= $modelClass ?> para las solicitudes en redis
*
* @package <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?> 
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{

    public $isModal = true;

    /**
    * @var Model $modelClass Modelo para las operaciones CRUD
    */
    public $modelClass = \<?= $generator->baseModelClass ?>::class;

    /**
    * @var Model $searchModel Modelo para las búsquedas
    */
    public $searchModelClass = \<?= $generator->searchModelClass ?>::class;


}