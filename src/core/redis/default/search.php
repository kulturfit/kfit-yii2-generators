<?php

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator kfit\generators\rest\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use <?= ltrim($generator->baseModelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;

/**
 * <?= $searchModelClass ?> represents the model behind the search form about `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass ?>

{
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \<?= ltrim($searchApp, '\\')?>::class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $defaultRules = [
            ['transaction', 'safe']
        ];
        $modelRules = [
            <?= implode(",\n            ", $rules) ?>,
        ];
        return Yii::$app->arrayHelper::merge($defaultRules, $modelRules);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['transaction' => $this->transaction]);

        <?= implode("\n        ", $searchConditions) ?>

        return $dataProvider;
    }
    
}
