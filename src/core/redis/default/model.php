<?php

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator kfit\generators\rest\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$modelAlias = $modelClass . 'Model';
    
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();
$defaultRules = $generator->generateRedisRules();

$modelObject = Yii::createObject($generator->modelClass);

$ignoreAttributes = [
    $modelObject::STATUS_COLUMN,
    $modelObject::CREATED_BY_COLUMN,
    $modelObject::CREATED_AT_COLUMN,
    $modelObject::UPDATED_BY_COLUMN,
    $modelObject::UPDATED_AT_COLUMN
];

$fields = [];
foreach ($generator->getColumnNames() as $column) {
    if (!in_array($column, $ignoreAttributes)) {
        $fields[] = $column;
    }
}

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->baseModelClass, '\\')) ?>;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;


/**
* <?= $modelClass ?> 
* 
* @package <?= StringHelper::dirname(ltrim($generator->baseModelClass, '\\')) ?> 
*
<?php foreach ($tableSchema->columns as $column) : ?>
* @property <?= "{$column->phpType} \${$column->name} " . str_replace(["\n", "\r"], [" ", " "], $column->comment) . "\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)) : ?>
<?php foreach ($relations as $name => $relation) : ?>
* @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . " Datos relacionados con modelo \"{$relation[1]}\"\n" ?>
<?php endforeach; ?>
<?php endif; ?>
*
* @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
* @copyright (c) 2020, KulturFit S.A.S.
* @version 0.0.1
*/
class <?= $modelClass ?> extends \kfit\core\redis\Model

{
    
     /**
     * @var [type] Undocumented variable
     */
    public static $modelClass = \<?= ltrim($generator->modelClass, '\\')?>::class;

    public $includeActionColumns = false;

    public function rules()
    {
        $newRules = [
            <?php foreach ($defaultRules as $rule) : ?>
    <?= $rule?>
            <?php endforeach; ?> 
        ];

        return Yii::$app->arrayHelper::merge($newRules, parent::rules());
    }

    <?php foreach ($relations as $name => $relation) : ?>
    /**
     * Definición de la relación con el modelo "<?= $name ?>".
     *
     * @return <?= "\\" . $generator->appNs . "\\" . $name . "\n" ?>
     */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }

<?php endforeach; ?>

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \kfit\core\base\ActionColumn::class,
                'isModal' => true,
                'controller' => '/<?=$moduleId?>/<?=$controllerId?>',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }
}
