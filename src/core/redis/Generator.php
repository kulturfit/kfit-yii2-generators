<?php

namespace kfit\generators\core\redis;

use Yii;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\helpers\StringHelper;

/**
 * @todo documentar código fuente y eliminar metodos innecesarios
 */
class Generator extends \kfit\generators\components\Generator
{

    public $modelClass          = 'kfit\adm\models\app\\';
    public $controllerClass     = 'kfit\adm\modules\redis\controllers\\';
    public $baseControllerClass = 'kfit\core\base\Controller';
    public $searchModelClass    = 'kfit\adm\modules\redis\models\searchs\\';
    public $baseModelClass      = 'kfit\adm\modules\redis\models\base\\';
    public $fieldCreateDateSync = 'created_at';
    public $fieldUpdateDateSync = 'updated_at';
    public $fieldStatusSync     = 'active';
    public $pathModels          = '@kfit/adm/models/app';
    public $pathModelsSearch    = '@kfit/adm/models/searchs';
    public $pathModule          = '@kfit/adm/modules';
    public $models              = [];
    public $module              = 'Redis';
    public $token               = 'O8IK2MRarQTr9qzgbpcOdn88-dmc4afQ';

    public $appNs                      = 'kfit\adm\models\app';



    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Redis';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Este generador crear un controlador con su respectivo modelo para proveer un servicio Web tipo API Rest.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['baseControllerClass'], 'filter', 'filter' => 'trim'],
                [
                    ['fieldCreateDateSync', 'fieldUpdateDateSync', 'fieldStatusSync', 'baseControllerClass'],
                    'required'
                ],
                [['models', 'module', 'pathModels', 'pathModule'], 'safe'],
                [
                    ['baseControllerClass'],
                    'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'
                ],
                [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'modelClass'          => 'Model Class',
                'controllerClass'     => 'Controller Class',
                'baseControllerClass' => 'Base Controller Class',
                'searchModelClass'    => 'Search Model Class',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(
            parent::hints(),
            [
                'modelClass'          => 'This is the ActiveRecord class associated with the table that CRUD will be built upon.
                You should provide a fully qualified class name, e.g., <code>app\models\Post</code>.',
                'controllerClass'     => 'This is the name of the controller class to be generated. You should
                provide a fully qualified namespaced class (e.g. <code>app\controllers\PostController</code>),
                and class name should be in CamelCase with an uppercase first letter. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
                'baseControllerClass' => 'This is the class that the new CRUD controller class will extend from.
                You should provide a fully qualified class name, e.g., <code>yii\web\Controller</code>.',
                'searchModelClass'    => 'This is the name of the search model class to be generated. You should provide a fully
                qualified namespaced class name, e.g., <code>app\models\PostSearch</code>.',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['controller.php'];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['baseControllerClass']);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {

        $files            = [];
        $moduleId = Inflector::camel2id($this->module, '');
        $fullPathModule = $this->joinPaths('\\', $this->pathModule, $moduleId);
        $pathControllers  =  $this->joinPaths('\\', $fullPathModule, 'controllers');
        $pathSearchModels = $this->joinPaths('\\', $fullPathModule, 'models\\searchs');
        $pathBaseModels = $this->joinPaths('\\', $fullPathModule, 'models\\app');
        $relations = $this->generateRelations();
        $db        = $this->getDbConnection();

        if (empty($this->models)) {
            return [];
        }

        foreach ($this->models as $model) {
            /* Nombres de las clases */
            $controllerName = Inflector::pluralize(Inflector::camelize(StringHelper::basename($model))) . 'Controller';
            $modelName      = Inflector::camelize(StringHelper::basename($model));


            /* Path de las clases */
            $this->modelClass = $this->normalizeClassPath($this->pathModels, $modelName);
            $searchModelAppClass = $this->normalizeClassPath($this->pathModelsSearch, $modelName);
            $this->controllerClass = $this->normalizeClassPath($pathControllers, $controllerName);
            $this->searchModelClass = $this->normalizeClassPath($pathSearchModels, $modelName);
            $this->baseModelClass = $this->normalizeClassPath($pathBaseModels, $modelName);

            $tableName = $this->modelClass::tableName();
            $tableSchema    = $db->getTableSchema($tableName);

            $relationNames = array_keys(isset($relations[$tableName]) ? $relations[$tableName] : []);
            $extraFields = [];

            foreach ($relationNames as $relationName) {
                $extraFields[] = Inflector::variablize($relationName);
            }

            $this->addCodeFile($files, $this->controllerClass, 'controller.php');
            $this->addCodeFile($files, $this->baseModelClass, 'model.php', [
                'tableSchema'    => $tableSchema,
                'extraFields' => $extraFields,
                'relations'      => isset($relations[$tableName]) ? $relations[$tableName] : [],
                'moduleId' => $moduleId,
                'controllerId' => Inflector::camel2id(Inflector::pluralize(Inflector::camelize(StringHelper::basename($model))))
            ]);
            $this->addCodeFile($files, $this->searchModelClass, 'search.php', [
                'searchApp' => $searchModelAppClass
            ]);
        }

        return $files;
    }


    /**
     * Returns the message to be displayed when the newly generated code is saved successfully.
     * Child classes may override this method to customize the message.
     * @return string the message to be displayed when the newly generated code is saved successfully.
     */
    public function successMessage()
    {

        $moduleId      = Inflector::camel2id($this->module);
        $controladores = "";
        foreach ($this->models as $model) {
            $modelName     = Inflector::pluralize(Inflector::camel2Id($model));
            $controladores .= "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'{$moduleId}/{$modelName}',";
        }
        $controladores = trim($controladores, ',');
        $resultado     = "[<br>
        &nbsp;&nbsp;&nbsp;&nbsp;'class' => 'yii\\rest\\UrlRule',<br>
        &nbsp;&nbsp;&nbsp;&nbsp;'controller' => [
        {$controladores}<br>
        &nbsp;&nbsp;&nbsp;&nbsp;],<br>
        &nbsp;&nbsp;&nbsp;&nbsp;'pluralize' => false<br>
        ]";
        return "Rules<pre style=\"color: black;\">" . $resultado . "</pre>";
    }

    public function generateRedisRules()
    {

        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }

        $db = $this->getDbConnection();
        $types = [];
        foreach ($table->foreignKeys as $refs) {
            $refTable       = $refs[0];
            $refTableSchema = $db->getTableSchema($refTable);
            if ($refTableSchema === null) {
                // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                continue;
            }
            unset($refs[0]);
            $fks          = array_keys($refs);
            $types['default'][]    = $fks[0];
        }
        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type', 'value'=>'0']";
        }


        return $rules;
    }

    /**
     * @return array the generated relation declarations
     */
    protected function generateRelations()
    {
        if ($this->generateRelations === self::RELATIONS_NONE) {
            return [];
        }

        $db = $this->getDbConnection();

        $relations = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $db->getTableSchema($refTable);
                    if ($refTableSchema === null) {
                        // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                        continue;
                    }
                    unset($refs[0]);
                    $fks          = array_keys($refs);
                    $refClassName = $this->generateClassName($refTable);

                    // Add relation for this table
                    $link                                       = $this->generateRelationLink(array_flip($refs));
                    $relationName                               = $this->generateRelationName(
                        $relations,
                        $table,
                        $fks[0],
                        false
                    );
                    $relations[$table->fullName][$relationName] = [
                        "return \$this->hasOne(Yii::\$container->get(\\{$this->appNs}\\{$refClassName}::class), $link)->cache(3);",
                        $refClassName,
                        false,
                    ];

                    // Add relation for the referenced table
                    $hasMany                                             = $this->isHasManyRelation(
                        $table,
                        $fks
                    );
                    $link                                                = $this->generateRelationLink($refs);
                    $relationName                                        = $this->generateRelationName(
                        $relations,
                        $refTableSchema,
                        $className,
                        $hasMany
                    );
                    $relations[$refTableSchema->fullName][$relationName] = [
                        $hasMany ? "\$query = \$this->hasMany(Yii::\$container->get(\\{$this->appNs}\\{$className}::class), $link);\n\t\t\$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);\n\t\treturn \$query;" : "return \$this->hasOne(Yii::\$container->get(\\{$this->appNs}\\{$className}::class), $link)->cache(3);",
                        $className,
                        $hasMany,
                    ];
                }

                if (($junctionFks = $this->checkJunctionTable($table)) === false) {
                    continue;
                }

                $relations = $this->generateManyManyRelations(
                    $table,
                    $junctionFks,
                    $relations
                );
            }
        }

        if ($this->generateRelations === self::RELATIONS_ALL_INVERSE) {
            return $this->addInverseRelations($relations);
        }
        return $relations;
    }

    /**
     * Adds inverse relations
     *
     * @param array $relations relation declarations
     * @return array relation declarations extended with inverse relation names
     * @since 2.0.5
     */
    protected function addInverseRelations($relations)
    {
        $relationNames = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($this->getDbConnection()->getSchema()->getTableSchemas($schemaName) as
                $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $this->getDbConnection()->getTableSchema($refTable);
                    unset($refs[0]);
                    $fks            = array_keys($refs);

                    $leftRelationName                                             = $this->generateRelationName(
                        $relationNames,
                        $table,
                        $fks[0],
                        false
                    );
                    $relationNames[$table->fullName][$leftRelationName]           = true;
                    $hasMany                                                      = $this->isHasManyRelation(
                        $table,
                        $fks
                    );
                    $rightRelationName                                            = $this->generateRelationName(
                        $relationNames,
                        $refTableSchema,
                        $className,
                        $hasMany
                    );
                    $relationNames[$refTableSchema->fullName][$rightRelationName] = true;

                    $relations[$table->fullName][$leftRelationName][0]           = rtrim(
                        $relations[$table->fullName][$leftRelationName][0],
                        ';'
                    )
                        . "->inverseOf('" . lcfirst($rightRelationName) . "');";
                    $relations[$refTableSchema->fullName][$rightRelationName][0] = rtrim(
                        $relations[$refTableSchema->fullName][$rightRelationName][0],
                        ';'
                    )
                        . "->inverseOf('" . lcfirst($leftRelationName) . "');";
                }
            }
        }
        return $relations;
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table   = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            $hashConditions[] = "'{$column}' => \$this->{$column},";
            // switch ($type) {
            //     case Schema::TYPE_SMALLINT:
            //     case Schema::TYPE_INTEGER:
            //     case Schema::TYPE_BIGINT:
            //     case Schema::TYPE_BOOLEAN:
            //     case Schema::TYPE_FLOAT:
            //     case Schema::TYPE_DOUBLE:
            //     case Schema::TYPE_DECIMAL:
            //     case Schema::TYPE_MONEY:
            //     case Schema::TYPE_DATE:
            //     case Schema::TYPE_TIME:
            //     case Schema::TYPE_DATETIME:
            //     case Schema::TYPE_TIMESTAMP:
            //         $hashConditions[] = "'{$column}' => \$this->{$column},";
            //         break;
            //     default:
            //         $likeConditions[] = "->andFilterWhere(['like', '{$column}', \$this->{$column}])";
            //         break;
            // }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode(
                    "\n" . str_repeat(' ', 12),
                    $hashConditions
                )
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode(
                "\n" . str_repeat(' ', 12),
                $likeConditions
            ) . ";\n";
        }

        return $conditions;
    }
}
