<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator kfit\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>
use Yii;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

 <?php if (!$generator->canUseModal()) : ?>
$this->title = $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
<?php endif ?>
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">

<?php if (!$generator->canUseModal()) : ?>
    <p>
        <?= "<?= " ?> UI::btnUpdate(null, ['update', <?= $urlParams ?>]); ?>
        <?= "<?= " ?> UI::btnDelete(null, ['delete', <?= $urlParams ?>]); ?>
    </p>
<?php endif ?>
    <?= "<?= " ?>DetailView::widget([
        'model' => $model,
        'attributes' => [
<?php
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        echo "            '" . $name . "',\n";
    }
} else {
    foreach ($generator->getTableSchema()->columns as $column) {
        $format = $generator->generateColumnFormat($column);

        if (isset($generator->foreignFields[$column->name])) {
            echo "            [
                'attribute' => '{$column->name}',
                'value'     => function (\$model, \$widget){
                    \$name = \$model->{$generator->foreignFields[$column->name]['relation']}->getNameFromRelations();

                    return \$model->{$generator->foreignFields[$column->name]['relation']}->\$name;
                },
            ],\n";
        } elseif ($column->name == $model::STATUS_COLUMN) {
            echo "            [
                'attribute' => '" . $model::STATUS_COLUMN . "',
                'value'     => Yii::\$app->strings::getCondition(\$model->" . $model::STATUS_COLUMN . "),
            ],\n";
        } else {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>
        ],
    ]) ?>

</div>
