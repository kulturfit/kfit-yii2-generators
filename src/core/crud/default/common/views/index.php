<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator kfit\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$controllerId = Inflector::camel2id(StringHelper::basename($generator->modelClass));

echo "<?php\n";
?>

use Yii;
use <?php echo $generator->indexWidgetType === 'grid' ? "kartik\dynagrid\DynaGrid" : "yii\\widgets\\ListView" ?>;
use yii\bootstrap4\Modal;
use kartik\grid\GridView;

/* @var $this yii\web\View */
<?php echo !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?php echo $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?php echo Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
        <?php if ($generator->indexWidgetType === 'grid') : ?>
<?php echo "<?php echo " ?>DynaGrid::widget([
            'columns' => [
                ['class'=>'kartik\grid\SerialColumn'],
                <?php
                $tableSchema = $generator->getTableSchema();
                $cont = 0;
                if ($tableSchema === false) {
                    foreach ($generator->getColumnNames() as $name) {
                        echo "\t\t\t\t[\n";
                        echo "\t\t\t\t\t'attribute' => '{$name}',\n";
                        echo "\t\t\t\t\t'visible' => " . ($generator->isColumnVisible($column) && $cont <= 6 ? 'true' : 'false') . ",\n";
                        echo "\t\t\t\t],\n";
                        $cont++;
                    }
                } else {
                    foreach ($tableSchema->columns as $column) {
                        $format = $generator->generateColumnFormat($column);
                        echo "\t\t\t\t[\n";
                        echo "\t\t\t\t\t'attribute' => '{$column->name}',\n";
                        if ($format !== 'text') {
                            echo "\t\t\t\t\t'format' => '{$format}',\n";
                        }
                        echo "\t\t\t\t\t'visible' => " . ($generator->isColumnVisible($column) && $cont <= 6 ? 'true' : 'false') . ",\n";

                        if (isset($generator->foreignFields[$column->name])) {
                            echo "\t\t\t\t\t'value' => function(\$model) {\n\t\t\t\t\t\tif (!empty(\$model->{$generator->foreignFields[$column->name]['relation']})) {\n\t\t\t\t\t\t\t\$name = \$model->{$generator->foreignFields[$column->name]['relation']}->getNameFromRelations();\n\t\t\t\t\t\t\treturn \$model->{$generator->foreignFields[$column->name]['relation']}->\$name;\n\t\t\t\t\t\t}\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => {$generator->foreignFields[$column->name]['class']}::getData(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Yii::\$app->strings::getTextAll()],\n";
                        } elseif ($column->name == $model::STATUS_COLUMN) {
                            echo "\t\t\t\t\t'value' => function(\$model){\n\t\t\t\t\t\treturn Yii::\$app->strings::getCondition(\$model->" . $model::STATUS_COLUMN . ");\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => Yii::\$app->strings::getCondition(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Yii::\$app->strings::getTextAll()],\n";
                            echo "\t\t\t\t\t'hiddenFromExport' => true,\n";
                        } elseif ($column->name == $model::CREATED_BY_COLUMN) {
                            echo "\t\t\t\t\t'value' => function(\$model){\n\t\t\t\t\t\treturn \$model->creadoPor->username;\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => Yii::\$app->userHelper::getUsers(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Yii::\$app->strings::getTextAll()],\n";
                            echo "\t\t\t\t\t'hiddenFromExport' => true,\n";
                        } elseif ($column->name == $model::UPDATED_BY_COLUMN) {
                            echo "\t\t\t\t\t'value' => function(\$model){\n\t\t\t\t\t\treturn \$model->modificadoPor->username;\n\t\t\t\t\t},\n";
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_SELECT2,\n";
                            echo "\t\t\t\t\t'filter' => Yii::\$app->userHelper::getUsers(),\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid', 'placeholder' => Yii::\$app->strings::getTextAll()],\n";
                            echo "\t\t\t\t\t'hiddenFromExport' => true,\n";
                        } elseif ($format == 'datetime' || $format == "date" || $column->name == $model::CREATED_AT_COLUMN || $column->name == $model::UPDATED_AT_COLUMN) {
                            echo "\t\t\t\t\t'filterType' => GridView::FILTER_DATE,\n";
                            echo "\t\t\t\t\t'filterInputOptions' => ['id' => '{$column->name}_grid'],\n";
                            echo "\t\t\t\t\t'hiddenFromExport' => true,\n";
                        }

                        echo "\t\t\t\t],\n";
                        $cont++;
                    }
                }
                ?>
                [
                    'class' => '<?= $generator->baseActionColumn ?>',
                    <?php
                    if ($generator->canUseModal()) {
                        echo "'isModal' => true,\n";
                        echo "\t\t\t\t\t'dynaModalId' => '{$controllerId}',\n";
                    }
                    ?>

                ],
            ],
            'gridOptions' => [
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'toolbar' => [
                    '{toggleData}',
                    [
                        'content' =>
                        <?php
                        if ($generator->canUseModal()) {
                            echo "\tYii::\$app->ui::btnNew('', ['create'], ['onClick' => \"openModalGrid(this, '{$controllerId}', 'create'); return false;\"]) . ' '\n";
                        } else {
                            echo "\tYii::\$app->ui::btnNew('') . ' '\n";
                        }
                        ?>
                            .Yii::$app->ui::btnSearch('') . ' '
                            .Yii::$app->ui::btnRefresh('')
                    ]
                ],
                'pjax' => true,
                'options' => ['id' => 'gridview-<?= $controllerId ?>'],
            ],
            'options' => ['id' => 'dynagrid-<?= $controllerId ?>'],
        ]) ?>
        <?php else : ?>
            <?php echo "<?php echo " ?>ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => function ($model, $key, $index, $widget) {
                    return Yii::$app->html::a(Yii::$app->html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                },
            ]) ?>
        <?php endif; ?>
</div>
<?php echo "<?php " ?>
Modal::begin([
    'id'      => 'search-modal',
    'title'  => Yii::t('app', 'Búsqueda Avanzada'),
    'size'    => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t('app', 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t('app', 'Buscar'),  ['form' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search-form']),
    'options' => [ 'style' => 'display: none;', 'tabindex' => '-1' ]
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
<?php echo "?>" ?>

<?php if ($generator->canUseModal()) : ?>
<?php echo "<?php " ?>Modal::begin([
    'id'            =>'<?= $controllerId ?>-modal',
    'title'        => '<?php echo Inflector::singularize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>',
    'size'    => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModalMessage(Yii::t('app', 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(null,  ['form' => '<?= $controllerId ?>-form']),
    'closeButton' => [ 'class' => 'close confirm-close' ],
    'options'       => ['style' => 'display: none;', 'tabindex' => '-1'],
]);

Modal::end();
<?php echo "?>" ?>

<?php endif; ?>
