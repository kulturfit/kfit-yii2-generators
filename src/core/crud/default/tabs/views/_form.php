<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator kfit\generators\crud\Generator */

echo "<?php\n\n";
?>
use <?= $generator->baseTabs ?>;
use <?= $generator->baseActiveForm ?>;
use <?= $generator->baseHelperUI ?>;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form <?= $generator->baseActiveForm ?> */

$form = ActiveForm::begin([
    'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form',
]);
?>
<div class="<?php echo Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">
    <?php echo "<?= " ?>Tabs::widget([
        'items' => [
            [
                'label'   => <?php echo $generator->generateString('Información') ?>,
                'content' => $this->render('1_informacion', ['form' => $form, 'model' => $model]),
                'active'  => true
            ],
        ],
    ]);
    ?>

    <div class="form-group pull-right">
        <?= "<?= " ?> UI::btnCancel(); ?>
        <?= "<?= " ?> UI::btnSend(); ?>
    </div>

    <?php echo "<?php " ?>ActiveForm::end(); ?>
</div>
