<?php

/**
 * Esta es una plantilla para generar un CRUD controller
 */

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator kfit\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use Yii;
use <?= ltrim($generator->baseControllerClassModal, '\\') ?>;

/**
* Controlador <?= $controllerClass ?> implementa las acciones para el CRUD de el modelo <?= $modelClass ?>.
*
* @package <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?> 
*
* @property string $modelClass Ruta del modelo principal.
* @property string $searchModelClass Ruta del modelo para la búsqueda.
*
* @author <?= Yii::$app->params['developers'][$generator->developer] ?> <<?= $generator->developer ?>>
* @copyright Copyright (c) <?= date('Y') . " {$generator->copyright} \n" ?>
* @version 0.0.1
* @since <?= $generator->since . "\n" ?>
*/
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClassModal) . "\n" ?>
{
    public $isModal = true;
    public $modelClass = \<?php echo $generator->modelClass ?>::class;
    public $searchModelClass = \<?php echo $generator->searchModelClass ?>::class;
}