<?php

namespace kfit\generators\core\crud;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Generador de CRUD
 *
 * @package kfit
 * @subpackage generators/crud
 * @category Generators
 *
 * @property array $columnNames Nombres de columna del Modelo. Esta propiedad es de sólo lectura.
 * @property string $controllerID El ID de controlador (sin el prefijo ID de módulo). Esta propiedad es de sólo lectura.
 * @property array $searchAttributes Atributos de búsqueda. Esta propiedad es de sólo lectura.
 * @property boolean|\yii\db\TableSchema $tableSchema Esta propiedad es de sólo lectura.
 * @property string $viewPath El camino vista controlador. Esta propiedad es de sólo lectura.
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class Generator extends \yii\gii\Generator
{
    use \kfit\generators\components\traits\TraitConfigurations;

    public $modelClass = 'app\models\app\\';
    public $controllerClass = 'app\controllers\\';
    public $baseControllerClass = 'kfit\core\base\Controller';
    public $baseControllerClassModal = 'kfit\core\base\Controller';
    public $indexWidgetType = 'grid';
    public $searchModelClass = 'app\models\searchs\\';
    public $ignoreFields = [];
    public $viewPath;
    public $enablePjax;
    public $models = [];
    public $pathModels = '@app/models/app';
    public $pathControllers = '@app/controllers';
    public $pathSearchModels = '@app/models/searchs';
    public $pathViews = '@app/views';
    public $enableI18N = true;
    public $developer;
    public $copyright;
    public $since = '1.0.0';
    public $useTabs = false;
    public $useModal = true;
    public $foreignFields = [];
    public $fieldMaxModal = 8;
    public $baseActiveForm = 'kfit\core\widgets\ActiveForm';
    public $baseTabs = 'kfit\core\widgets\Tabs';
    public $baseHelperHtml = 'kfit\core\helpers\HtmlHelper';
    public $baseHelperUI = 'kfit\core\helpers\UIHelper';
    public $baseHelperStrings = 'kfit\core\helpers\StringsHelper';
    public $baseHelperUsuario = 'kfit\core\helpers\UserHelper';
    public $baseActionColumn = 'kfit\core\base\ActionColumn';

    /**
     * Inicializa el generador
     */
    public function init()
    {
        parent::init();

        $this->ignoreFields = [
            'activo',
            'creado_por',
            'fecha_creacion',
            'modificado_por',
            'fecha_modificacion',
            'active',
            'created_by',
            'modified_by',
            'created_at',
            'modified_at'
        ];
    }

    /**
     * Valida si las columna debe ser visible o no
     *
     * @param \yii\db\ColumnSchema|string $column Columna que se desea buscar
     * @return boolean
     */
    public function isColumnVisible($column)
    {
        if (isset($column->isPrimaryKey)) {
            return !$column->isPrimaryKey && !in_array(
                $column->name,
                $this->ignoreFields
            );
        } else {
            return !in_array($column, $this->ignoreFields);
        }
    }

    /**
     * Entrega los modelos disponibles en el directorio
     *
     * @return array
     */
    public function getModelFiles()
    {
        $files = [];
        if (file_exists(Yii::getAlias($this->pathModels)) && is_dir(Yii::getAlias($this->pathModels))) {
            foreach (scandir(Yii::getAlias($this->pathModels)) as $file) {
                if (is_file(Yii::getAlias($this->pathModels) . "/{$file}") && pathinfo(
                    $file,
                    PATHINFO_EXTENSION
                ) === 'php') {
                    $files[$file] = $file;
                } elseif ($file !== '.' && $file !== '..' && is_dir(Yii::getAlias($this->pathModels) . "/{$file}")) {
                    foreach (scandir(Yii::getAlias($this->pathModels) . "/{$file}") as
                        $subFile) {
                        if (is_file(Yii::getAlias($this->pathModels) . "/{$file}/{$subFile}") && pathinfo(
                            $subFile,
                            PATHINFO_EXTENSION
                        ) === 'php') {
                            $files["{$file}/{$subFile}"] = "{$file}/{$subFile}";
                        }
                    }
                }
            }
        }
        return $files;
    }

    /**
     * Entrega el nombre del generador
     * @return string
     */
    public function getName()
    {
        return "CRUD's";
    }

    /**
     * Entrega la descripcion del generador
     * @return string
     */
    public function getDescription()
    {
        return 'Crea todas las funcionalidades necesarias para administrar (crear, ver, actualizar, eliminar), los registros de una tabla de la base de datos conectada o del modelo definido.';
    }

    /**
     * Entrega las reglas de validación
     * @return array
     */
    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [
                    ['pathModels', 'pathControllers', /* 'pathSearchModels', */ 'pathViews'],
                    'validatePath', 'skipOnEmpty' => false
                ],
                [[
                    'pathModels', 'models', 'pathControllers', 'pathSearchModels', 'pathViews',
                    'developer'
                ], 'required'],
                [['enableI18N'], 'boolean'],
                [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
                [['indexWidgetType'], 'in', 'range' => ['grid', 'list']],
                [['copyright','models', 'useTabs', 'useModal'], 'safe'],
            ]
        );
    }

    /**
     * Valida que el directoro pasado sea un directorio existente
     *
     * @return boolean
     */
    public function validatePath($attribute, $params)
    {
        if (!file_exists(Yii::getAlias($this->$attribute)) || !is_dir(Yii::getAlias($this->$attribute))) {
            $this->addError($attribute, 'Ruta del direcctorio incorrecto.');
        }
    }

    /**
     * Entrega los lables de los atributos del modelo
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'modelClass' => 'Model Class',
                'controllerClass' => 'Controller Class',
                'viewPath' => 'View Path',
                'baseControllerClass' => 'Base Controller Class',
                'developer' => 'Desarrollador Autor',
                'indexWidgetType' => 'Widget Used in Index Page',
                'searchModelClass' => 'Search Model Class',
                'pathViews' => 'Path Views',
                'useTabs' => 'Usar Tabs',
                'useModal' => 'Usar Modal',
            ]
        );
    }

    /**
     * Entrega las notas por cada label
     * @return array
     */
    public function hints()
    {
        return array_merge(
            parent::hints(),
            [
                'baseControllerClass' => 'Esta es la clase que la nueva clase controlador CRUD se extenderá.
                Usted debe proporcionar un nombre de clase completo con namespace, e.g., <code>app\component\Controller</code>.',
                'baseControllerClass' => 'Esta es la clase que la nueva clase controlador CRUD en Ventanas modales se extenderá.
                Usted debe proporcionar un nombre de clase completo con namespace, e.g., <code>app\component\ControllerModal</code>.',
                'developer' => 'Los datos del desarrollador que aparecerá en el tag @autor de la clase',
                'indexWidgetType' => 'Este es el tipo de widget para ser utilizado en la página de índice a la lista de los modelos de visualización <code>GridView</code> o <code>ListView</code>',
                'searchModelClass' => 'Este es el nombre de la clase del modelo de búsqueda que será generado.'
                    . ' Usted debe proporcionar un nombre de clase completo con namespace e.g., <code>app\models\PostSearch</code>.',
                'pathModels' => 'Ruta de los modelos',
                'pathControllers' => 'Ruta de los controladores a ser generados.',
                'pathSearchModels' => 'Ruta de los modelos search a ser generados.',
                'pathViews' => 'Ruta de las vistas a ser generadas',
                'useTabs' => 'Usar formularios con tabs para formularios mayores a ' . $this->fieldMaxModal . ' campos.',
                'useModal' => 'Usar formularios en ventana modal para formularios menores o iguales a ' . $this->fieldMaxModal . ' campos',
            ]
        );
    }

    /**
     * Plantillas requeridas
     * @return array
     */
    public function requiredTemplates()
    {
        return [];
    }

    /**
     * Atributos que son inicializados con algún valor por defecto
     * @return array
     */
    public function stickyAttributes()
    {
        return array_merge(
            parent::stickyAttributes(),
            ['baseControllerClass', 'indexWidgetType']
        );
    }

    /**
     * Verifica si la clase modelo es válida
     */
    public function validateModelClass()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();
        if (empty($pk)) {
            $this->addError(
                'modelClass',
                "The table associated with $class must have primary key(s)."
            );
        }
    }

    /**
     * Entrega el máximo de campos a tener en cuenta para ventanas modales
     *
     * @return integer
     */
    public function getMaxFieldsModal()
    {
        return $this->fieldMaxModal - (count($this->ignoreFields));
    }

    /**
     * Método que indica si se puede usar ventanas modales o no en la CRUD
     *
     * @return boolean
     */
    public function canUseModal()
    {
        $model = new $this->modelClass();
        $columns = $model->getTableSchema()->columns;

        foreach ($columns as $key => $column) {
            if (in_array($column->name, $this->ignoreFields) || $column->autoIncrement) {
                unset($columns[$key]);
            }
        }

        return $this->useModal && count($columns) <= $this->fieldMaxModal;
    }

    /**
     * Generate a relation name for the specified table and a base name.
     * Genera el nombre de una relación basadi en el nombre de la tabla y el campo
     *
     * @param \yii\db\TableSchema $table Escquema de la tabla
     * @param string $key Nombre base de la relación
     *
     * @return string Nombre de la relación
     */
    protected function generateRelationName($table, $key)
    {
        if (!empty($key) && substr_compare($key, 'id', -2, 2, true) === 0 && strcasecmp(
            $key,
            'id'
        )) {
            $key = rtrim(substr($key, 0, -2), '_');
        }

        $name = $rawName = lcfirst(Inflector::id2camel($key, '_'));
        $i = 0;
        while (isset($table->columns[$name])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * Método encargado de armar las llaver foraneas disponibles
     */
    private function setForeignFields()
    {
        $model = new $this->modelClass();

        foreach ($model->getTableSchema()->foreignKeys as $refs) {
            $refTable = $refs[0];
            $refTableSchema = Yii::$app->db->getTableSchema($refTable);
            if ($refTableSchema === null) {
                //Si el Foreign key no encuentra la tabla de referencia
                continue;
            }
            unset($refs[0]);

            if (($pos = strrpos($refTable, '.')) !== false) {
                $refTable = substr($refTable, $pos + 1);
            }

            $fks = array_keys($refs);
            $refClassName = Inflector::id2camel($refTable, '_');
            $modelClassArray = explode("\\", $this->modelClass);
            unset($modelClassArray[count($modelClassArray) - 1]);
            $modelClassArray = implode("\\", $modelClassArray);
            $modelClass = "{$modelClassArray}\\{$refClassName}";

            $this->foreignFields[$fks[0]] = [
                'class' => $modelClass,
                'relation' => $this->generateRelationName(
                    $model->getTableSchema(),
                    $fks[0]
                ),
            ];
        }
    }

    /**
     * Genera el CRUD basado en su configuración
     *
     * @return CodeFile[]
     */
    public function generate()
    {
        $files = [];
        foreach ($this->models as $model) {
            /* Nombres de las clases */
            $controllerName = Inflector::camelize(StringHelper::basename(str_replace(
                '.php',
                '',
                $model
            ))) . 'Controller';
            $searchModelName = Inflector::camelize(StringHelper::basename(str_replace(
                '.php',
                '',
                $model
            )));

            /* Path de las clases */
            $this->modelClass = str_replace(
                ['@', '/'],
                ['', '\\'],
                $this->pathModels
            ) . '\\';
            if (strpos($model, '/')) {
                $sub = explode('/', $model);
                $this->modelClass .= "{$sub[0]}\\";
            }
            $this->modelClass .= Inflector::camelize(StringHelper::basename(str_replace(
                '.php',
                '',
                $model
            )));
            $this->controllerClass = str_replace(
                ['@', '/'],
                ['', '\\'],
                $this->pathControllers
            ) . '\\' . $controllerName;
            $this->searchModelClass = str_replace(
                ['@', '/'],
                ['', '\\'],
                $this->pathSearchModels
            ) . '\\' . Inflector::camelize(StringHelper::basename(str_replace(
                '.php',
                '',
                $model
            )));

            $this->foreignFields = [];
            $this->setForeignFields();
            $canUseModal = $this->canUseModal();

            /* Controller file */
            $controllerFile = Yii::getAlias($this->pathControllers . '/' . $controllerName . '.php');
            $files[$controllerFile] = new CodeFile(
                $controllerFile,
                $this->render(($canUseModal ? 'modal/' : 'normal/') . 'controller.php')
            );

            /* Search model file */
            $searchModelFile = Yii::getAlias($this->pathSearchModels . '/' . $searchModelName . '.php');
            $files[$searchModelFile] = new CodeFile(
                $searchModelFile,
                $this->render('common/search.php')
            );

            /* Views Common */
            // $viewPath = Yii::getAlias($this->pathViews . '/' . $this->getControllerID());
            // $baseTemplate = 'common/views';
            // $templatePath = "{$this->getTemplatePath()}/{$baseTemplate}";

            // $files = $this->addFiles(
            //     $templatePath,
            //     $baseTemplate,
            //     $viewPath,
            //     $files
            // );

            // if ($canUseModal) {
            //     /* Views Modal */
            //     $baseTemplate = 'modal/views';
            //     $templatePath = "{$this->getTemplatePath()}/{$baseTemplate}";

            //     $files = $this->addFiles(
            //         $templatePath,
            //         $baseTemplate,
            //         $viewPath,
            //         $files
            //     );
            // } else {
            //     /* Views Normal */
            //     $baseTemplate = 'normal/views';
            //     $templatePath = "{$this->getTemplatePath()}/{$baseTemplate}";

            //     $files = $this->addFiles(
            //         $templatePath,
            //         $baseTemplate,
            //         $viewPath,
            //         $files
            //     );

            //     if ($this->useTabs) {
            //         $baseTemplate = 'tabs/views';
            //         $templatePath = "{$this->getTemplatePath()}/{$baseTemplate}";

            //         /* Views Tabs */
            //         $baseTemplate = 'tabs/views';
            //         $templatePath = "{$this->getTemplatePath()}/{$baseTemplate}";

            //         $files = $this->addFiles(
            //             $templatePath,
            //             $baseTemplate,
            //             $viewPath,
            //             $files
            //         );
            //     }
            // }
        }

        return $files;
    }

    /**
     * Agrega archivos al listado de archivos actual basado en el template necesario
     *
     * @param string $templatePath Ruta a escanear para los views
     * @param string $baseTemplate Template base
     * @param string $viewPath
     * @param CodeFile[] $files
     *
     * @return CodeFile[]
     */
    private function addFiles($templatePath, $baseTemplate, $viewPath, $files)
    {
        foreach (scandir($templatePath) as $file) {
            if ($file === '_search.php' && empty($this->searchModelClass)) {
                continue;
            }
            if (is_file($templatePath . '/' . $file) && pathinfo(
                $file,
                PATHINFO_EXTENSION
            ) === 'php') {
                $files["{$viewPath}/{$file}"] = new CodeFile(
                    "$viewPath/$file",
                    $this->render(
                        "{$baseTemplate}/{$file}",
                        ['model' => new $this->modelClass()]
                    )
                );
            }
        }

        return $files;
    }

    /**
     * Retorna la template seleccionado
     *
     * @return string Ruta base de los archivos de plantilla que serán usados.
     * @throws InvalidConfigException si [[template]] es inválido
     */
    public function getTemplatePath()
    {
        $template = 'default';
        if ($this->template != 'default') {
            if (isset($this->templates[$this->template])) {
                $template = $this->template;
            } else {
                throw new InvalidConfigException("Unknown template: {$this->template}");
            }
        } else {
            $columns = $this->getColumnNames();
            $columnsNum = 0;
            foreach ($columns as $column) {
                if (array_search($this->getPrimaryKey(), $columns)) {
                    $columnsNum++;
                }
            }
        }

        return $this->templates[$template];
    }

    /**
     * @return string El ID de controlador (sin el prefijo ID de módulo).
     */
    public function getControllerID()
    {
        $pos = strrpos($this->controllerClass, '\\');
        $class = substr(substr($this->controllerClass, $pos + 1), 0, -10);

        return Inflector::camel2id($class);
    }

    /**
     * @return string Ruta de las vistas del Controller
     */
    public function getViewPath()
    {
        if (empty($this->viewPath)) {
            return Yii::getAlias('@app/views/' . $this->getControllerID());
        } else {
            return Yii::getAlias($this->viewPath);
        }
    }

    /**
     * @return string entrega el nameAttribute del modelo
     */
    public function getNameAttribute()
    {
        foreach ($this->getColumnNames() as $name) {
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title') || !strcasecmp(
                $name,
                'nombre'
            ) || !strcasecmp(
                $name,
                'titulo'
            )) {
                return $name;
            }
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();

        return $pk[0];
    }

    /**
     * Genera código para el active field
     *
     * @param string $attribute Atributo del modelo
     * @param string $index Indice del input para navegación
     * @return string
     */
    public function generateActiveField($attribute, $index)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            $field = "\$form->field(\$model, '$attribute')";
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return $field . "->passwordInput(['tabindex'=>$index])";
            } else {
                return $field;
            }
        }

        $column = $tableSchema->columns[$attribute];
        $help = $this->getHelp($column);
        $field = "\$form->field(\$model, '$attribute'$help)";
        if ($column->phpType === 'boolean') {
            return $field . "->checkbox(['tabindex'=>$index])";
        } elseif ($column->type === 'text') {
            return $field . "->textarea(['rows' => 6,'tabindex'=>$index])";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }

            if (isset($this->foreignFields[$column->name])) {
                return $field . "->widget(\kartik\widgets\Select2::classname(),
                        [
                            'data' => {$this->foreignFields[$column->name]['class']}::getData(),
                            'options' => ['placeholder' => Yii::\$app->strings::getTextEmpty(), 'tabindex' => $index],
                        ]
                    )";
            } elseif ($column->type == 'date') {
                return $field . "->widget(\kartik\widgets\DatePicker::classname(),['options' => ['tabindex'=>$index]])";
            } elseif ($column->type == 'time') {
                return $field . "->widget(\kartik\widgets\TimePicker::classname(), ['options' => ['tabindex'=>$index]])";
            } elseif ($column->type == 'datetime') {
                return $field . "->widget(\kartik\widgets\DateTimePicker::classname(), ['options' => ['tabindex'=>$index]])";
            } elseif (is_array($column->enumValues) && count($column->enumValues) > 0) {
                $dropDownOptions = [];
                foreach ($column->enumValues as $enumValue) {
                    $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
                }

                return $field . "->widget(\kartik\widgets\Select2::classname()
                        [
                            'data' => " . preg_replace(
                    "/\n\s*/",
                    ' ',
                    VarDumper::export($dropDownOptions)
                ) . ",
                            'options' => ['placeholder' => Yii::\$app->strings::getTextEmpty(), 'tabindex'=>$index]
                        ]
                    )";
            } elseif ($column->phpType !== 'string' || $column->size === null) {
                return $field . "->$input(['tabindex'=>$index])";
            } else {
                return $field . "->$input(['maxlength' => true, 'tabindex'=>$index])";
            }
        }
    }

    /**
     * Método encargado de etregar la estructura para help si la tiene
     *
     * @param \yii\db\ColumnSchema $column
     * @return string
     */
    private function getHelp($column)
    {
        $help = "";

        if ($column->comment != '') {
            $help = ",['help' => '','popover' => \$model->getHelp('{$column->name}')]";
        }

        return $help;
    }

    /**
     * Genera el codigo para los campos de busqueda
     *
     * @param string $attribute Atributo del modelo
     * @param string $index Indice del input para navegación
     * @return string
     */
    public function generateActiveSearchField($attribute, $index)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false) {
            return "\$form->field(\$model, '$attribute')";
        }
        $column = $tableSchema->columns[$attribute];
        $help = $this->getHelp($column);
        $field = "\$form->field(\$model, '$attribute'$help)";
        $defaultOptions = "'tabindex' => $index, 'id' => '{$attribute}_search'";

        if (isset($this->foreignFields[$column->name])) {
            return $field . "->widget(\kartik\widgets\Select2::classname(),
                    [
                        'data' => {$this->foreignFields[$column->name]['class']}::getData(),
                        'options' => ['placeholder' => Yii::\$app->strings::getTextAll(), $defaultOptions],
                    ]
                )";
        } elseif ($attribute === $this->modelClass::STATUS_COLUMN) {
            return $field . "->widget(\kartik\widgets\Select2::classname(),
                        [
                            'data' => Yii::\$app->strings::getCondition(),
                            'options' => ['placeholder' => Yii::\$app->strings::getTextAll(), $defaultOptions],
                        ]
                    )";
        } elseif ($attribute === $this->modelClass::CREATED_BY_COLUMN || $attribute === $this->modelClass::UPDATED_BY_COLUMN) {
            return $field . "->widget(\kartik\widgets\Select2::classname(),
                        ['data' => Yii::\$app->userHelper::getUsers(), 'options' => ['placeholder' => Yii::\$app->strings::getTextEmpty(), $defaultOptions]]
                    )";
        } elseif ($column->phpType === 'boolean') {
            return $field . "->checkbox([$defaultOptions])";
        } elseif ($column->type == 'date' || ($attribute === $this->modelClass::CREATED_AT_COLUMN || $attribute === $this->modelClass::UPDATED_AT_COLUMN)) {
            return $field . "->widget(\kartik\widgets\DatePicker::classname(), ['options' => [$defaultOptions]])";
        } elseif ($column->type == 'time') {
            return $field . "->widget(\kartik\widgets\TimePicker::classname(), ['options' => [$defaultOptions]])";
        } elseif ($column->type == 'datetime') {
            return $field . "->widget(\kartik\widgets\DateTimePicker::classname(), ['options' => [$defaultOptions]])";
        } else {
            return $field . "->textInput([$defaultOptions])";
        }
    }

    /**
     * Generates column format
     * @param \yii\db\ColumnSchema $column
     * @return string
     */
    public function generateColumnFormat($column)
    {
        if ($column->phpType === 'boolean') {
            return 'boolean';
        } elseif ($column->type === 'text') {
            return 'ntext';
        } elseif ((stripos($column->name, 'time') !== false && $column->phpType === 'integer') || ($column->name == 'fecha_creacion' || $column->name == 'fecha_modificacion')) {
            return 'datetime';
        } elseif (stripos($column->name, 'fecha') !== false) {
            return 'date';
        } elseif (stripos($column->name, 'email') !== false) {
            return 'email';
        } elseif (stripos($column->name, 'url') !== false) {
            return 'url';
        } else {
            return 'text';
        }
    }

    /**
     * Generates validation rules for the search model.
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }
        $types = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                    $types['integer'][] = $column->name;
                    break;
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $types['number'][] = $column->name;
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                default:
                    $types['safe'][] = $column->name;
                    break;
            }
        }

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }

        return $rules;
    }

    /**
     * @return array searchable attributes
     */
    public function getSearchAttributes()
    {
        return $this->getColumnNames();
    }

    /**
     * Generates the attribute labels for the search model.
     * @return array the generated attribute labels (name => label)
     */
    public function generateSearchLabels()
    {
        /* @var $model \yii\base\Model */
        $model = new $this->modelClass();
        $attributeLabels = $model->attributeLabels();
        $labels = [];
        foreach ($this->getColumnNames() as $name) {
            if (isset($attributeLabels[$name])) {
                $labels[$name] = $attributeLabels[$name];
            } else {
                if (!strcasecmp($name, 'id')) {
                    $labels[$name] = 'Código';
                } else {
                    $label = Inflector::camel2words($name);
                    if (!empty($label) && substr_compare(
                        $label,
                        ' id',
                        -3,
                        3,
                        true
                    ) === 0) {
                        $label = 'Código';
                    }
                    $labels[$name] = $label;
                }
            }
        }

        return $labels;
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            switch ($type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                    $hashConditions[] = "'{$column}' => \$this->{$column},";
                    break;
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "'DATE({$column})' => \$this->{$column},";
                    break;
                default:
                    $likeConditions[] = "->andFilterWhere(['ilike', '{$column}', \$this->{$column}])";
                    break;
            }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode(
                "\n" . str_repeat(' ', 12),
                $hashConditions
            )
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode(
                "\n" . str_repeat(' ', 12),
                $likeConditions
            ) . ";\n";
        }

        return $conditions;
    }

    /**
     * Generates URL parameters
     * @return string
     */
    public function generateUrlParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (count($pks) === 1) {
            if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                return "'id' => (string)\$model->{$pks[0]}";
            } else {
                return "'id' => \$model->{$pks[0]}";
            }
        } else {
            $params = [];
            foreach ($pks as $pk) {
                if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                    $params[] = "'$pk' => (string)\$model->$pk";
                } else {
                    $params[] = "'$pk' => \$model->$pk";
                }
            }

            return implode(', ', $params);
        }
    }

    /**
     * Generates action parameters
     * @return string
     */
    public function generateActionParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (count($pks) === 1) {
            return '$id';
        } else {
            return '$' . implode(', $', $pks);
        }
    }

    /**
     * Generates parameter tags for phpdoc
     * @return array parameter tags for phpdoc
     */
    public function generateActionParamComments()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (($table = $this->getTableSchema()) === false) {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . (substr(strtolower($pk), -2) == 'id' ? 'integer' : 'string') . ' $' . $pk;
            }

            return $params;
        }
        if (count($pks) === 1) {
            return ['@param ' . $table->columns[$pks[0]]->phpType . ' $id'];
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . $table->columns[$pk]->phpType . ' $' . $pk;
            }

            return $params;
        }
    }

    /**
     * Returns table schema for current model class or false if it is not an active record
     * @return boolean|\yii\db\TableSchema
     */
    public function getTableSchema()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema();
        } else {
            return false;
        }
    }

    /**
     * @return array model column names
     */
    public function getColumnNames()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema()->getColumnNames();
        } else {
            /* @var $model \yii\base\Model */
            $model = new $class();

            return $model->attributes();
        }
    }

    /**
     * @return string model column names
     */
    public function getPrimaryKey()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            $objClass = new $this->modelClass;
            return $objClass->primaryKey;
        }
    }

}
