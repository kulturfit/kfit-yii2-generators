<?php
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator kfit\generators\crud\Generator */
echo $form->field($generator, 'pathModels');
echo $form->field($generator, 'models')->dropDownList($generator->getModelFiles(), [
    'multiple' => true
]);
echo $form->field($generator, 'pathControllers');
echo $form->field($generator, 'pathSearchModels');
echo $form->field($generator, 'pathViews');

echo $form->field($generator, 'developer')->dropDownList($generator->getDevelopers());
echo $form->field($generator, 'copyright');
echo $form->field($generator, 'since');
echo $form->field($generator, 'indexWidgetType')->dropDownList([
    'grid' => 'GridView',
    'list' => 'ListView',
]);
echo $form->field($generator, 'enableI18N')->checkbox();
echo $form->field($generator, 'messageCategory');
echo $form->field($generator, 'baseControllerClass');
echo $form->field($generator, 'baseControllerClassModal');
echo $form->field($generator, 'useTabs')->checkbox();
echo $form->field($generator, 'useModal')->checkbox();
