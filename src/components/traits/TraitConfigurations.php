<?php

namespace kfit\generators\components\traits;

use Yii;

/**
 * Trait para implementar algunas configuraciones comunes en las clases
 *
 * @package kfit
 * @subpackage generators\components\trait
 * @category
 *
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.

 */
trait TraitConfigurations
{
    /**
     * Retorna los desarrolladores disponibles para asignarlos como autores.
     *
     * @param string $key Correo electrónico para retornar el nombre.
     * @return array|string
     */
    public function getDevelopers($key = null)
    {
        $defaults = [
            'developers@kulturfit.com' => 'KulturFit developer'
        ];
        $developers = isset(Yii::$app->params['developers']) ? array_merge(Yii::$app->params['developers'], $defaults) : $defaults;
        $returnValue = $developers;
        if (!is_null($key) && isset($developers[$key])) {
            $returnValue = $developers[$key];
        }
        return $returnValue;
    }
}