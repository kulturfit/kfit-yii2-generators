<?php

namespace kfit\generators\components;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\helpers\StringHelper;
use kfit\core\helpers\ArrayHelper;
use yii\base\NotSupportedException;

/**
 * Undocumented class
 */
class Generator extends \yii\gii\Generator
{
    use \kfit\generators\components\traits\TraitConfigurations;

    const RELATIONS_NONE        = 'none';
    const RELATIONS_ALL         = 'all';
    const RELATIONS_ALL_INVERSE = 'all-inverse';

    public $generateRelations          = self::RELATIONS_ALL;
    public $db                         = 'db';
    public $tableName;
    protected $classNames;

    public $modelClass          = 'app\models\base\\';
    public $controllerClass     = 'app\modules\v1\controllers\\';
    public $baseControllerClass = 'kfit\core\rest\ActiveController';
    public $searchModelClass    = 'app\modules\v1\models\searchs\\';
    public $baseModelClass      = 'app\modules\v1\models\base\\';
    public $fieldCreateDateSync = 'created_at';
    public $fieldUpdateDateSync = 'updated_at';
    public $fieldStatusSync     = 'active';
    public $pathModels          = '@app/models/base';
    public $models              = [];
    public $developer;
    public $copyright = 'KulturFit S.A.S.';
    public $since     = '1.0.0';

    public $useTablePrefix = false;

    /**
     * @return Connection the DB connection as specified by [[db]].
     */
    protected function getDbConnection()
    {
        return Yii::$app->get($this->db, false);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Base';
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        return [];
    }


    /**
     * @return string[] all db schema names or an array with a single empty string
     * @throws NotSupportedException
     * @since 2.0.5
     */
    protected function getSchemaNames()
    {
        $db     = $this->getDbConnection();
        $schema = $db->getSchema();
        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }
        return $schemaNames;
    }

    /**
     * Generates a class name from the specified table name.
     * @param string $tableName the table name (which may contain schema prefix)
     * @param boolean $useSchemaName should schema name be included in the class name, if present
     * @return string the generated class name
     */
    protected function generateClassName($tableName, $useSchemaName = null)
    {
        if (isset($this->classNames[$tableName])) {
            return $this->classNames[$tableName];
        }

        $schemaName    = '';
        $fullTableName = $tableName;
        if (($pos           = strrpos($tableName, '.')) !== false) {
            if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                $schemaName = substr($tableName, 0, $pos) . '_';
            }
            $tableName = substr($tableName, $pos + 1);
        }

        $db         = $this->getDbConnection();
        $patterns   = [];
        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        $patterns[] = "/^(.*?){$db->tablePrefix}$/";
        if (strpos($this->tableName, '*') !== false) {
            $pattern = $this->tableName;
            if (($pos     = strrpos($pattern, '.')) !== false) {
                $pattern = substr($pattern, $pos + 1);
            }
            $patterns[] = '/^' . str_replace('*', '(\w+)', $pattern) . '$/';
        }
        $className = $tableName;
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $tableName, $matches)) {
                $className = $matches[1];
                break;
            }
        }

        return $this->classNames[$fullTableName] = Inflector::id2camel(
            $schemaName . $className,
            '_'
        );
    }


    /**
     * Checks if model class is valid
     */
    public function validateModelClass()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pk    = $class::primaryKey();
        if (empty($pk)) {
            $this->addError(
                'modelClass',
                "The table associated with $class must have primary key(s)."
            );
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $fileList
     * @param [type] $basePath
     * @param [type] $templateName
     * @return void
     */
    protected function addCodeFile(&$fileList, $basePath, $templateName, $params = [], $extension = 'php')
    {
        if (!empty($basePath)) {
            $finalBasePath = Yii::getAlias('@' . str_replace(
                '\\',
                '/',
                ltrim(
                    $basePath,
                    '\\'
                )
            ));

            $fileList[] = new CodeFile(
                $finalBasePath . ".{$extension}",
                $this->render($templateName, $params)
            );
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $basePath
     * @param [type] $className
     * @return void
     */
    protected function normalizeClassPath($basePath, $className)
    {
        return str_replace(
            ['@', '/'],
            ['', '\\'],
            $basePath
        ) . '\\' . $className;
    }

    /**
     * @return string the controller ID (without the module ID prefix)
     */
    public function getControllerID()
    {
        $pos   = strrpos($this->controllerClass, '\\');
        $class = substr(substr($this->controllerClass, $pos + 1), 0, -10);

        return Inflector::camel2id($class);
    }

    /**
     * @return string the controller view path
     */
    public function getViewPath()
    {
        if (empty($this->viewPath)) {
            return Yii::getAlias('@app/views/' . $this->getControllerID());
        } else {
            return Yii::getAlias($this->viewPath);
        }
    }

    public function getNameAttribute()
    {
        foreach ($this->getColumnNames() as $name) {
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title')) {
                return $name;
            }
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->modelClass;
        $pk    = $class::primaryKey();

        return $pk[0];
    }



    /**
     * Generates code for active field
     * @param string $attribute
     * @return string
     */
    public function generateActiveField($attribute)
    {
        $tableSchema = $this->getTableSchema();


        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model, '$attribute')";
            }
        }
        $column = $tableSchema->columns[$attribute];
        $help   = $this->getHelp($column);
        if ($column->phpType === 'boolean') {
            return "\$form->field(\$model, '$attribute'$help)->checkbox()";
        } elseif ($column->type === 'text') {
            return "\$form->field(\$model, '$attribute'$help)->textarea(['rows' => 6])";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if (is_array($column->enumValues) && count($column->enumValues) > 0) {
                $dropDownOptions = [];
                foreach ($column->enumValues as $enumValue) {
                    $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
                }
                return "\$form->field(\$model, '$attribute'$help)->dropDownList("
                    . preg_replace(
                        "/\n\s*/",
                        ' ',
                        VarDumper::export($dropDownOptions)
                    ) . ", ['prompt' => ''])";
            } elseif ($column->phpType !== 'string' || $column->size === null) {
                return "\$form->field(\$model, '$attribute'$help)->$input()";
            } else {
                return "\$form->field(\$model, '$attribute'$help)->$input(['maxlength' => true])";
            }
        }
    }

    protected function getHelp($column)
    {
        $help = "";

        if ($column->comment != '') {
            $comment = explode('||', $column->comment);
            if (count($comment) == 2) {
                $comment[0] = trim($comment[0]);
                $comment[1] = trim($comment[1]);

                $help .= ",['help'=>'{$comment[1]}','popover'=>'{$comment[0]}']";
            }
        }

        return $help;
    }

    /**
     * Generates code for active search field
     * @param string $attribute
     * @return string
     */
    public function generateActiveSearchField($attribute)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false) {
            return "\$form->field(\$model, '$attribute')";
        }
        $column = $tableSchema->columns[$attribute];
        $help   = $this->getHelp($column);
        if ($column->phpType === 'boolean') {
            return "\$form->field(\$model, '$attribute'$help)->checkbox()";
        } else {
            return "\$form->field(\$model, '$attribute'$help)";
        }
    }

    /**
     * Generates column format
     * @param \yii\db\ColumnSchema $column
     * @return string
     */
    public function generateColumnFormat($column)
    {
        if ($column->phpType === 'boolean') {
            return 'boolean';
        } elseif ($column->type === 'text') {
            return 'ntext';
        } elseif (stripos($column->name, 'time') !== false && $column->phpType === 'integer') {
            return 'datetime';
        } elseif (stripos($column->name, 'email') !== false) {
            return 'email';
        } elseif (stripos($column->name, 'url') !== false) {
            return 'url';
        } else {
            return 'text';
        }
    }

    /**
     * Generates validation rules for the search model.
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }
        $types = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                    $types['integer'][] = $column->name;
                    break;
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $types['number'][]  = $column->name;
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                default:
                    $types['safe'][]    = $column->name;
                    break;
            }
        }

        $types['safe'][] = 'lastDate';
        $types['safe'][] = 'notIn';

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }


        return $rules;
    }

    /**
     * @return array searchable attributes
     */
    public function getSearchAttributes()
    {
        return $this->getColumnNames();
    }

    /**
     * Generates the attribute labels for the search model.
     * @return array the generated attribute labels (name => label)
     */
    public function generateSearchLabels()
    {
        /* @var $model \yii\base\Model */
        $model           = new $this->modelClass();
        $attributeLabels = $model->attributeLabels();
        $labels          = [];
        foreach ($this->getColumnNames() as $name) {
            if (isset($attributeLabels[$name])) {
                $labels[$name] = $attributeLabels[$name];
            } else {
                if (!strcasecmp($name, 'id')) {
                    $labels[$name] = 'ID';
                } else {
                    $label = Inflector::camel2words($name);
                    if (!empty($label) && substr_compare(
                        $label,
                        ' id',
                        -3,
                        3,
                        true
                    ) === 0) {
                        $label = substr($label, 0, -3) . ' ID';
                    }
                    $labels[$name] = $label;
                }
            }
        }

        return $labels;
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table   = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            switch ($type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "'{$column}' => \$this->{$column},";
                    break;
                default:
                    $likeConditions[] = "->andFilterWhere(['like', '{$column}', \$this->{$column}])";
                    break;
            }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode(
                    "\n" . str_repeat(' ', 12),
                    $hashConditions
                )
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode(
                "\n" . str_repeat(' ', 12),
                $likeConditions
            ) . ";\n";
        }

        return $conditions;
    }

    /**
     * Generates URL parameters
     * @return string
     */
    public function generateUrlParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks   = $class::primaryKey();
        if (count($pks) === 1) {
            if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                return "'id' => (string)\$model->{$pks[0]}";
            } else {
                return "'id' => \$model->{$pks[0]}";
            }
        } else {
            $params = [];
            foreach ($pks as $pk) {
                if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                    $params[] = "'$pk' => (string)\$model->$pk";
                } else {
                    $params[] = "'$pk' => \$model->$pk";
                }
            }

            return implode(', ', $params);
        }
    }

    /**
     * Generates action parameters
     * @return string
     */
    public function generateActionParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks   = $class::primaryKey();
        if (count($pks) === 1) {
            return '$id';
        } else {
            return '$' . implode(', $', $pks);
        }
    }

    /**
     * Generates parameter tags for phpdoc
     * @return array parameter tags for phpdoc
     */
    public function generateActionParamComments()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks   = $class::primaryKey();
        if (($table = $this->getTableSchema()) === false) {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . (substr(strtolower($pk), -2) == 'id' ? 'integer' : 'string') . ' $' . $pk;
            }

            return $params;
        }
        if (count($pks) === 1) {
            return ['@param ' . $table->columns[$pks[0]]->phpType . ' $id'];
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . $table->columns[$pk]->phpType . ' $' . $pk;
            }

            return $params;
        }
    }

    /**
     * Returns table schema for current model class or false if it is not an active record
     * @return boolean|\yii\db\TableSchema
     */
    public function getTableSchema()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema();
        } else {
            return false;
        }
    }

    /**
     * @return array model column names
     */
    public function getColumnNames()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema()->getColumnNames();
        } else {
            /* @var $model \yii\base\Model */
            $model = new $class();

            return $model->attributes();
        }
    }

    public function getPrimaryKey()
    {
        $primaryKey = null;
        /* @var $class ActiveRecord */
        $class      = $this->modelClass;
        $pks        = $class::primaryKey();

        if (count($pks) > 0) {
            $primaryKey = $pks[0];
        }

        return $primaryKey;
    }



    /**
     * Registra un directorio en busqueda de todos los archivos
     *
     * @return Array
     */
    public function getModelFiles()
    {
        $listaArchivos = [];
        if (file_exists(Yii::getAlias($this->pathModels)) && is_dir(Yii::getAlias($this->pathModels))) {
            foreach (scandir(Yii::getAlias($this->pathModels)) as $file) {
                if (is_file(Yii::getAlias($this->pathModels) . '/' . $file) && pathinfo(
                    $file,
                    PATHINFO_EXTENSION
                ) === 'php' && !strpos(
                    $file,
                    'Search.php'
                )) {

                    $rename     = str_replace('.php', '', $file);
                    $modelClass = str_replace(
                        ['@', '/'],
                        ['', '\\'],
                        $this->pathModels
                    ) . '\\' . $rename;

                        if (is_subclass_of($modelClass, 'yii\db\ActiveRecord')) {
                            $listaArchivos[$rename] = $rename;
                        }
                }
            }
        }
        return $listaArchivos;
    }

    /**
     * Undocumented function
     *
     * @param [type] $model
     * @return void
     */
    public function getDescriptionModel($model)
    {
        $sql = "SELECT obj_description('{$model::tableName()}'::regclass, 'pg_class')";
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    /**
     * Pruebas relations
     */

    /**
     * @return array the generated relation declarations
     */
    protected function generateRelations()
    {
        if ($this->generateRelations === self::RELATIONS_NONE) {
            return [];
        }

        $db = $this->getDbConnection();

        $relations = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $db->getTableSchema($refTable);
                    if ($refTableSchema === null) {
                        // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                        continue;
                    }
                    unset($refs[0]);
                    $fks          = array_keys($refs);
                    $refClassName = $this->generateClassName($refTable);

                    // Add relation for this table
                    $link                                       = $this->generateRelationLink(array_flip($refs));
                    $relationName                               = $this->generateRelationName(
                        $relations,
                        $table,
                        $fks[0],
                        false
                    );
                    $relations[$table->fullName][$relationName] = [
                        "return \$this->hasOne($refClassName::className(), $link);",
                        $refClassName,
                        false,
                    ];

                    // Add relation for the referenced table
                    $hasMany                                             = $this->isHasManyRelation(
                        $table,
                        $fks
                    );
                    $link                                                = $this->generateRelationLink($refs);
                    $relationName                                        = $this->generateRelationName(
                        $relations,
                        $refTableSchema,
                        $className,
                        $hasMany
                    );
                    $relations[$refTableSchema->fullName][$relationName] = [
                        $hasMany ? "\$query = \$this->hasMany($className::className(), $link);\n\t\t\$query->andWhere([self::STATUS_COLUMN => self::STATUS_ACTIVE]);\n\t\treturn \$query;" : "return \$this->hasOne($className::className(), $link);",
                        $className,
                        $hasMany,
                    ];
                }

                if (($junctionFks = $this->checkJunctionTable($table)) === false) {
                    continue;
                }

                $relations = $this->generateManyManyRelations(
                    $table,
                    $junctionFks,
                    $relations
                );
            }
        }

        if ($this->generateRelations === self::RELATIONS_ALL_INVERSE) {
            return $this->addInverseRelations($relations);
        }
        return $relations;
    }

    /**
     * Adds inverse relations
     *
     * @param array $relations relation declarations
     * @return array relation declarations extended with inverse relation names
     * @since 2.0.5
     */
    protected function addInverseRelations($relations)
    {
        $relationNames = [];
        foreach ($this->getSchemaNames() as $schemaName) {
            foreach ($this->getDbConnection()->getSchema()->getTableSchemas($schemaName) as
                $table) {
                $className = $this->generateClassName($table->fullName);
                foreach ($table->foreignKeys as $refs) {
                    $refTable       = $refs[0];
                    $refTableSchema = $this->getDbConnection()->getTableSchema($refTable);
                    unset($refs[0]);
                    $fks            = array_keys($refs);

                    $leftRelationName                                             = $this->generateRelationName(
                        $relationNames,
                        $table,
                        $fks[0],
                        false
                    );
                    $relationNames[$table->fullName][$leftRelationName]           = true;
                    $hasMany                                                      = $this->isHasManyRelation(
                        $table,
                        $fks
                    );
                    $rightRelationName                                            = $this->generateRelationName(
                        $relationNames,
                        $refTableSchema,
                        $className,
                        $hasMany
                    );
                    $relationNames[$refTableSchema->fullName][$rightRelationName] = true;

                    $relations[$table->fullName][$leftRelationName][0]           = rtrim(
                        $relations[$table->fullName][$leftRelationName][0],
                        ';'
                    )
                        . "->inverseOf('" . lcfirst($rightRelationName) . "');";
                    $relations[$refTableSchema->fullName][$rightRelationName][0] = rtrim(
                        $relations[$refTableSchema->fullName][$rightRelationName][0],
                        ';'
                    )
                        . "->inverseOf('" . lcfirst($leftRelationName) . "');";
                }
            }
        }
        return $relations;
    }

    /**
     * Determines if relation is of has many type
     *
     * @param TableSchema $table
     * @param array $fks
     * @return boolean
     * @since 2.0.5
     */
    protected function isHasManyRelation($table, $fks)
    {
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDbConnection()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        foreach ($uniqueKeys as $uniqueKey) {
            if (count(array_diff(
                array_merge($uniqueKey, $fks),
                array_intersect($uniqueKey, $fks)
            )) === 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generates the link parameter to be used in generating the relation declaration.
     * @param array $refs reference constraint
     * @return string the generated link parameter.
     */
    protected function generateRelationLink($refs)
    {
        $pairs = [];
        foreach ($refs as $a => $b) {
            $pairs[] = "'$a' => '$b'";
        }

        return '[' . implode(', ', $pairs) . ']';
    }

    /**
     * Checks if the given table is a junction table, that is it has at least one pair of unique foreign keys.
     * @param \yii\db\TableSchema the table being checked
     * @return array|boolean all unique foreign key pairs if the table is a junction table,
     * or false if the table is not a junction table.
     */
    protected function checkJunctionTable($table)
    {
        if (count($table->foreignKeys) < 2) {
            return false;
        }
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDbConnection()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        $result      = [];
        // find all foreign key pairs that have all columns in an unique constraint
        $foreignKeys = array_values($table->foreignKeys);
        for ($i = 0; $i < count($foreignKeys); $i++) {
            $firstColumns = $foreignKeys[$i];
            unset($firstColumns[0]);

            for ($j = $i + 1; $j < count($foreignKeys); $j++) {
                $secondColumns = $foreignKeys[$j];
                unset($secondColumns[0]);

                $fks = array_merge(
                    array_keys($firstColumns),
                    array_keys($secondColumns)
                );
                foreach ($uniqueKeys as $uniqueKey) {
                    if (count(array_diff(
                        array_merge($uniqueKey, $fks),
                        array_intersect(
                            $uniqueKey,
                            $fks
                        )
                    )) === 0) {
                        // save the foreign key pair
                        $result[] = [$foreignKeys[$i], $foreignKeys[$j]];
                        break;
                    }
                }
            }
        }
        return empty($result) ? false : $result;
    }

    /**
     * Generate a relation name for the specified table and a base name.
     * @param array $relations the relations being generated currently.
     * @param \yii\db\TableSchema $table the table schema
     * @param string $key a base name that the relation name may be generated from
     * @param boolean $multiple whether this is a has-many relation
     * @return string the relation name
     */
    protected function generateRelationName($relations, $table, $key, $multiple)
    {
        if (!empty($key) && substr_compare($key, 'id', -2, 2, true) === 0 && strcasecmp(
            $key,
            'id'
        )) {
            $key = rtrim(substr($key, 0, -2), '_');
        }
        if ($multiple) {
            $key = Inflector::pluralize($key);
        }
        $name    = $rawName = Inflector::id2camel($key, '_');
        $i       = 0;
        while (isset($table->columns[lcfirst($name)])) {
            $name = $rawName . ($i++);
        }
        while (isset($relations[$table->fullName][$name])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * Undocumented function
     *
     * @param string $conector
     * @param [type] ...$paths
     * @return void
     */
    protected function joinPaths($conector = '\\', ...$paths)
    {
        $fullPath = '';
        foreach ($paths as $path) {
            if (empty($fullPath)) {
                $fullPath = $path;
            } else {
                $fullPath .= "{$conector}{$path}";
            }
        }
        return $fullPath;
    }

    /**
     * Generates relations using a junction table by adding an extra viaTable().
     * @param \yii\db\TableSchema the table being checked
     * @param array $fks obtained from the checkJunctionTable() method
     * @param array $relations
     * @return array modified $relations
     */
    private function generateManyManyRelations($table, $fks, $relations)
    {
        $db = $this->getDbConnection();

        foreach ($fks as $pair) {
            list($firstKey, $secondKey) = $pair;
            $table0       = $firstKey[0];
            $table1       = $secondKey[0];
            unset($firstKey[0], $secondKey[0]);
            $className0   = $this->generateClassName($table0);
            $className1   = $this->generateClassName($table1);
            $table0Schema = $db->getTableSchema($table0);
            $table1Schema = $db->getTableSchema($table1);

            $link                                              = $this->generateRelationLink(array_flip($secondKey));
            $viaLink                                           = $this->generateRelationLink($firstKey);
            $relationName                                      = $this->generateRelationName(
                $relations,
                $table0Schema,
                key($secondKey),
                true
            );
            $relations[$table0Schema->fullName][$relationName] = [
                "\$query = \$this->hasMany(Yii::\$container->get(\\{$this->modelClass}\\{$className1}::class), $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', $viaLink);\n
                \$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE]);\n
                \n
                return \$query;",
                $className1,
                true,
            ];

            $link                                              = $this->generateRelationLink(array_flip($firstKey));
            $viaLink                                           = $this->generateRelationLink($secondKey);
            $relationName                                      = $this->generateRelationName(
                $relations,
                $table1Schema,
                key($firstKey),
                true
            );
            $relations[$table1Schema->fullName][$relationName] = [
                "\$query = \$this->hasMany(Yii::\$container->get(\\{$this->modelClass}\\{$className0}::class), $link)->viaTable('"
                    . $this->generateTableName($table->name) . "', $viaLink);\n
                \$query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE]);\n
                \n
                return \$query;",
                $className0,
                true,
            ];
        }

        return $relations;
    }

    /**
     * Generates the table name by considering table prefix.
     * If [[useTablePrefix]] is false, the table name will be returned without change.
     * @param string $tableName the table name (which may contain schema prefix)
     * @return string the generated table name
     */
    public function generateTableName($tableName)
    {
        if (!$this->useTablePrefix) {
            return $tableName;
        }

        $db = $this->getDbConnection();
        if (preg_match("/^{$db->tablePrefix}(.*?)$/", $tableName, $matches)) {
            $tableName = '{{%' . $matches[1] . '}}';
        } elseif (preg_match("/^(.*?){$db->tablePrefix}$/", $tableName, $matches)) {
            $tableName = '{{' . $matches[1] . '%}}';
        }
        return $tableName;
    }
}
