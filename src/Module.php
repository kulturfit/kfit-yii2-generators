<?php

namespace kfit\generators;

/**
* Inicializador de módulo.
*
* @package kfit
* @subpackage generators
* @category Module Config
*
* @property string $controllerNamespace Namespace de los controladores
*
* @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
* @copyright Copyright (c) 2018 KulturFit S.A.S.
* @version 0.0.1
* @since 2.0.0
*/
class Module extends \yii\gii\Module
{
    public $controllerNamespace = 'kfit\generators\controllers';

    /**
     * Retorna la lista de los generadores disponibles.
     *
     * @return array
     */
    protected function coreGenerators()
    {
        return [
            'model' => ['class' => 'kfit\generators\core\model\Generator'],
            'crud' => ['class' => 'kfit\generators\core\crud\Generator'],
            'rest' => ['class' => 'kfit\generators\core\rest\Generator'],
            'graphql' => ['class' => 'kfit\generators\core\graphql\Generator']
        ];
    }
}